const { UserStatus } = require("../variables")

function validateStatus(status){ // Validasi status user
    return async function (req, res, next){
        const { _user: user } = req.body
        // Happy Path
        if(user.status == status){ // Kalau sesuai lanjut
            return next()
        }
        // Sad Path
        let message = '';
        switch(user.status){
            case UserStatus.IDLE: 
                message = 'User is currently idle'
                break
            case UserStatus.EXPEDITION: 
                message = 'User is currently on expedition'
                break
            case UserStatus.MATCHING: 
                message = 'User is currently matching'
                break
            case UserStatus.MULTIPLAYER: 
                message = 'User is currently battling'
                break
            case UserStatus.ROOM: 
                message = 'User is currently on room'
                break
        }
        if(message != ''){
            return res.status(400).json({ message })
        }else{
            return res.status(400).json({
                message: 'User middleware status error'
            })
        }
    }
}

module.exports = validateStatus
