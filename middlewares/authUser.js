// Import
const User = require("../models/user");
const jwt = require("jsonwebtoken");
require('dotenv').config()

// Middleware
async function authUser(req, res, next) {
	if (!req.headers["x-auth-token"]) { // Jika tidak ada token
		return res.status(401).send({
			msg: "Unauthorized Data",
		});
	} else { // Jika ada Token
		let token = req.headers["x-auth-token"];
		try {
			var data = jwt.verify(token, process.env.JWT_SECRET);
            // Get User
            const { username } = data
            const user = await User.findOne({ username })
            if(user == null){ // Jika user tidak ditemukan
                return res.status(403).json({
                    error: 'invalid token'
                })
            }else{ // Jika User ketemu
                req.body._user = user;
                next();
            }
		} catch (error) { // Jika Token Invalid
			return res.send("invalid token");
		}
	}
}

module.exports = authUser
