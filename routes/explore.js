// Library (Bawaan)
const express = require('express')
const { isValidObjectId } = require('mongoose')

// Modules
const { authUser, validateStatus } = require('../middlewares')
const { Expedition, Egg, Item, PokedexPokemon, ExpeditionTrainer } = require('../models')
const { validateItemBattle, validateUserHasItem, useRestorationItem, usePokeball, switchPokemon, validateActionSwitch, validateBattlePokemon } = require('../modules/battle')
const { getRandomFloat, getRandomInt } = require('../modules/math')
const { generatePokemon, levelUpPokemon, giveExpPokemon } = require('../modules/pokeFactory')
const { UserStatus, ExpeditionType, Gender, BattleAction, BattleResult, ItemType } = require('../variables')

// Router
const router = express.Router()

// POST /explore
router.post('/', authUser, validateStatus(UserStatus.IDLE), async (req, res) => {
	let {
		expedition_id, // Expedition ID
		pokemons, // List ID Pokemonnya
		_user: user
	} = req.body
	// Check Remaining Explore
	if (!user.isPremium && user.remainingExplore <= 0) {
		return res.status(400).json({
			msg: 'Remaining explore 0!'
		})
	}
	// Check expedition
	if (!isValidObjectId(expedition_id)) {
		return res.status(400).json({
			msg: 'Expedition ID is not valid Object ID!'
		})
	}
	const expedition = await Expedition.findById(expedition_id)
	if (expedition == null) {
		return res.status(404).json({
			msg: 'Expedition not found!'
		})
	}
	// Check Inputan Kembar
	const isPokemonvalid = validateBattlePokemon(res, user, pokemons)
	if(!isPokemonvalid) return;
	// Get All Pokemon
	const userPokemons = user.pokemons.filter(poke => poke.isActive)
	const battlePokemons = userPokemons.filter(poke => pokemons.includes(poke._id + ''))
	// Random Expedition Drop
	const { drop } = expedition
	const {
		egg: eggRate,
		item: itemRate,
		wildPokemon: pokemonRate,
		trainer: trainerRate,
	} = drop.rate

	const idx = Math.random() * (eggRate + itemRate + pokemonRate + trainerRate)
	if (idx <= eggRate) { // Egg
		//// Random Index Egg (Ratio)
		const listRateEgg = drop.eggs.map(egg => egg.rate)
		listRateEgg.forEach((rate, idx) => {
			if (idx > 0) listRateEgg[idx] += listRateEgg[idx - 1]
		})
		const randomEggVal = getRandomFloat(0, listRateEgg[listRateEgg.length - 1])
		let idxEgg = 0
		while (randomEggVal >= listRateEgg[idxEgg]) {
			idxEgg++
		}
		//// End of Random Index Egg (Ratio)
		const eggId = drop.eggs[idxEgg].eggId
		const eggTemplate = await Egg.findById(eggId)
		const {
			name: eggName,
			description: eggDescription,
			images: eggImages
		} = eggTemplate
		const image = eggImages[getRandomInt(0, eggImages.length - 1)] // Random Image Egg
		const { min: minExp, max: maxExp } = eggTemplate.exploreRequirement
		const exploreRemaining = getRandomInt(minExp, maxExp)
		const newEgg = {
			eggId,
			image,
			exploreRemaining
		}
		// Push Egg
		user.eggs.push(newEgg);
		// Kurangi Explore Remaining
		updateExploreRemaining(user)
		// Tambahi History
		const newHistory = {
			expeditionId: expedition_id,
			date: new Date(),
			drop: ExpeditionType.EGG,
			pokemons: battlePokemons,
			egg: { eggId, image },
		}
		user.expeditions.push(newHistory)
		// Save User
		await user.save()
		return res.status(200).json({
			dailyExploreRemaining: user.remainingExplore,
			drop: ExpeditionType.EGG,
			egg: {
				name: eggName,
				description: eggDescription,
				image,
				exploreRemaining
			},
		})
	} else if (idx <= eggRate + itemRate) { // Item
		const items = drop.items.map(item => {
			const { min, max } = item.qty
			const qty = getRandomInt(min, max)
			return { itemId: item.itemId, qty }
		}).filter(item => item.qty > 0)
		if (items.length <= 0) { // Jika random tidak dapat item sama sekali
			const newItem = { itemId: items[0].itemId, qty: 1 }
			items.push(newItem)
		}
		// Get Mongo Doc
		for (let i = 0; i < items.length; i++) {
			const item = items[i]
			item.obj = await Item.findById(item.itemId)
		}

		// Insert Item QTY User
		items.forEach(item => {
			const { itemId, qty } = item
			const idxItem = user.items.findIndex(it => it.itemId.equals(itemId))
			if (idxItem >= 0) { // Sudah punya maka Update
				user.items[idxItem].qty += item.qty
			} else {
				user.items.push({ itemId, qty })
			}
		})
		// Kurangi Explore Remaining
		updateExploreRemaining(user)
		// Tambahi history
		const newHistory = {
			expeditionId: expedition_id,
			date: new Date(),
			drop: ExpeditionType.ITEM,
			pokemons: battlePokemons,
			items: items.map(item => ({ itemId: item.itemId, amount: item.qty })),
		}
		user.expeditions.push(newHistory)
		// Save User info
		await user.save()
		return res.status(200).json({
			dailyExploreRemaining: user.remainingExplore,
			drop: ExpeditionType.ITEM,
			items: items.map(item => ({
				...item.obj._doc,
				_id: undefined,
				__v: undefined,
				qty: item.qty
			}))
		})
	} else if (idx <= eggRate + itemRate + pokemonRate) { // Wild Pokemon
		const wildPokemon = await generateWildExpeditionPokemon(expedition)
		const { _id: historyId } = await insertWildPokeExpedition(user, expedition_id, battlePokemons, wildPokemon)

		// Update User Status
		status = BattleResult.ONGOING
		const expeditionBattle = {
			historyId,
			expeditionType: ExpeditionType.WILDPOKEMON,
			player: {
				activePokemon: battlePokemons[0],
				inactivePokemons: battlePokemons.filter((_, idx) => idx > 0)
			},
			moves: [],
			wildPokemon,
			status
		}
		user.expeditionBattle = expeditionBattle
		user.status = UserStatus.EXPEDITION

		await user.save()
		// Return
		return res.status(200).json({
			dailyExploreRemaining: user.remainingExplore,
			drop: ExpeditionType.WILDPOKEMON,
			wildPokemon: await mapPokeEnemy(wildPokemon)
		})
	} else {
		const trainer = await generateWildTrainer(expedition)
		const { _id: historyId } = await insertWildTrainerExpedition(user, expedition_id, battlePokemons, trainer)

		// Update User Status
		status = BattleResult.ONGOING
		const expeditionBattle = {
			historyId,
			expeditionType: ExpeditionType.TRAINER,
			player: {
				activePokemon: battlePokemons[0],
				inactivePokemons: battlePokemons.slice(1)
			},
			moves: [],
			trainer,
			status
		}
		user.expeditionBattle = expeditionBattle
		user.status = UserStatus.EXPEDITION

		await user.save()
		// Return
		return res.status(200).json({
			dailyExploreRemaining: user.remainingExplore,
			drop: ExpeditionType.TRAINER,
			trainer
		})
	}
})

// GET /explore/status
router.get('/status', authUser, validateStatus(UserStatus.EXPEDITION), async (req, res) => {
	const { _user: user } = req.body
	res.status(200).json({
		...user.expeditionBattle,
	})
})

// POST /explore/action
router.post('/action', authUser, validateStatus(UserStatus.EXPEDITION), async (req, res) => {
	let { 
		action, // Dalam angka
		itemId, // Jika action adalah item
		pokemonId, // Jika action adalah switch
		_user:user 
	} = req.body
	action = parseInt(action)
	// Check if action valid
	const possibleAction = Object.values(BattleAction)
	if(!possibleAction.includes(action)){
		return res.status(400).json({
			msg: 'Invalid action!'
		})
	}
	// Do Action
	const expeditionBattle = user.expeditionBattle
	if(expeditionBattle.status == BattleResult.ONGOING){ // Jika masih battle
		let actPoke = expeditionBattle.player.activePokemon
		let oppPoke = null
		if(expeditionBattle.expeditionType == ExpeditionType.WILDPOKEMON){
			oppPoke = expeditionBattle.wildPokemon
		}else{
			oppPoke = expeditionBattle.trainer.pokemons.find(poke => poke.hp > 0)
		}

		// Untuk Create Move Log
		function createLog(log){
			const move = {
				index: expeditionBattle.moves.length+1,
				log
			}
			user.expeditionBattle.moves.push(move)
			console.log(move)
		}

		// Jika poke player mati
		if(actPoke.hp <= 0){
			if(action == BattleAction.SWITCH){
				const isValid = validateActionSwitch(res, pokemonId, expeditionBattle.player)
				if(!isValid) return;
				const newPoke = switchPokemon(expeditionBattle.player, pokemonId)
				createLog(`Player choose ${newPoke.name}!`)
				user.markModified('expeditions')
				user.markModified('expeditionBattle')
				await user.save()
				return res.status(200).json({
					msg: 'successfully perform action!',
					action: BattleAction.toString(action),
					moves: user.expeditionBattle.moves
				})	
			}else{ // Jika action selain switch
				return res.status(400).json({
					msg: `Player can only choose pokemon!`
				})
			}
		}

		//// Validate Move
		// Validate End Battle
		if(action == BattleAction.ENDBATTLE){ // Tidak valid
			return res.status(400).json({
				msg: 'Invalid move for non-ended battle!'
			})
		}

		// validate SWITCH
		if(action == BattleAction.SWITCH){
			const isValid = validateActionSwitch(res, pokemonId, expeditionBattle.player)
			if(!isValid) return;
		}

		// validate ITEM
		if(action == BattleAction.ITEM){
			if(itemId == null){
				return res.status(400).json({
					msg: `Missing itemId field!`
				})
			}
			if(!validateUserHasItem(user, itemId)){ // Validate User punya item
				return res.status(400).json({
					msg: `User doesn't have that item!`
				})
			}
			const isWildPokemon = expeditionBattle.expeditionType == ExpeditionType.WILDPOKEMON
			if((await validateItemBattle(itemId, isWildPokemon)) == false){ // Validate User punya item
				return res.status(400).json({
					msg: 'That item cant be used in battle!'
				})
			}
		}
		// Validate Special Attack
		if(action == BattleAction.SPECIAL && actPoke.sp <= 0){
			return res.status(400).json({
				msg: 'Pokemon doesn\'t have enough sp!'
			})
		}

		// Dapetin yg gerak dulu
		// Player bergerak dulu jika mau run/switch atau spd lebih besar
		const isPlayerMoveFirst = 
			action == BattleAction.SWITCH ||
			action == BattleAction.RUN ||
			action == BattleAction.ITEM ||
			actPoke.spd > oppPoke.spd;

		// Action yang dijalankan enemy
		async function enemyAction(){
			if(expeditionBattle.status != BattleResult.ONGOING){// Check End Battle
				return;
			}
			if(oppPoke.hp <= 0) return;

			// 30% menggunakan special attack
			const isSpecialAtk = oppPoke.sp > 0 && Math.random() < 0.3
			let dmg = 0
			if(!isSpecialAtk){
				dmg = oppPoke.atk - actPoke.def
				createLog(`${oppPoke.name} use basic attack!`)
			}else{
				oppPoke.sp--;
				dmg = oppPoke.spAtk + oppPoke.atk - actPoke.spDef
				createLog(`${oppPoke.name} use special attack!`)
			}
			dmg = Math.max(1, dmg)
			actPoke.hp -= dmg
			createLog(`${actPoke.name} took ${dmg} damage!`)

			// Check darah musuh
			if(actPoke.hp <= 0){
				createLog(`${actPoke.name} fainted!`)
				if(isPlayerLoseBattle(expeditionBattle)){
					createLog(`Player run out of pokemon!`)
					expeditionBattle.status = BattleResult.LOSEBATTLE;
				}
			}
		}
		async function playerAction(){
			if(expeditionBattle.status != BattleResult.ONGOING){// Check End Battle
				return;
			}
			if(actPoke.hp <= 0) return;

			if(action == BattleAction.RUN){
				const isSuccessRun = Math.random() < 0.5
				if(isSuccessRun){
					expeditionBattle.status = BattleResult.RUN
					createLog('Successfully run from battle!')
				}else{
					createLog('Failed to run from battle!')
				}
			}else if(action == BattleAction.SWITCH){ // Harus ada Pokemon
				const newPoke = switchPokemon(expeditionBattle.player, pokemonId)
				createLog(`Player choose ${newPoke.name}!`)
			}else if(action == BattleAction.ITEM){ // Use Item
				const item = await Item.findById(itemId)
				createLog(`Player use ${item.name}!`)
				if(item.type == ItemType.POKEBALL){ // Jika catch pokemon
					await usePokeball(user, itemId, oppPoke, (isCaught)=>{
						if(isCaught){
							expeditionBattle.status = BattleResult.CATCHPOKEMON
							createLog(`${oppPoke.name} caught!`)
						}else{
							createLog(`Failed to catch ${oppPoke.name}!`)
						}
					})
				}else{ // Jika resorative item
					await useRestorationItem(user, itemId, actPoke, (amountHp, amountSp)=>{
						if(item.type == ItemType.POTION){
							createLog(`${actPoke.name} gain ${amountHp} hp!`)
						}else{
							createLog(`${actPoke.name} gain ${amountSp} sp!`)
						}
					})
				}
			}else{
				let dmg = 0
				if(action == BattleAction.ATTACK){
					dmg = actPoke.atk - oppPoke.def
					createLog(`${actPoke.name} use basic attack!`)
				}else if(action == BattleAction.SPECIAL){
					dmg = actPoke.spAtk + actPoke.atk - oppPoke.spDef
					actPoke.sp--
					createLog(`${actPoke.name} use special attack!`)
				} 
				oppPoke.hp -= dmg
				dmg = Math.max(1, dmg)
				createLog(`${oppPoke.name} took ${dmg} damage!`)

				// Check darah musuh
				if(oppPoke.hp <= 0){
					createLog(`${oppPoke.name} fainted!`)
					if(isEnemyLosebattle(expeditionBattle)){
						if(expeditionBattle.expeditionType == ExpeditionType.TRAINER){
							createLog(`${expeditionBattle.trainer.name} defeated!`)
						}
						expeditionBattle.status = BattleResult.WINBATTLE;
					}else if(expeditionBattle.expeditionType == ExpeditionType.TRAINER){
						// Switch Pokemon
						const tempPoke = expeditionBattle.trainer.pokemons.find(poke => poke.hp > 0)
						createLog(`Enemy choose ${tempPoke.name}!`)
					}
				}
			}
		}

		// Call Action
		if(!isPlayerMoveFirst) {
			await enemyAction()
			await playerAction()
		}else{
			await playerAction()
			await enemyAction()
		}
		
		// Update
		user.markModified('expeditions')
		user.markModified('expeditionBattle')
		user.markModified('items')
		user.markModified('pokemons')
		await user.save()

		// Return
		return res.status(200).json({
			msg: 'successfully perform action!',
			action: BattleAction.toString(action),
			moves: user.expeditionBattle.moves
		}) 
	}else { // Battle sudah selesai
		if(action == BattleAction.ENDBATTLE){ // Jika action user end battle
			const isWinBattle = expeditionBattle.status == BattleResult.WINBATTLE
			const isWildPokemon = expeditionBattle.expeditionType == ExpeditionType.WILDPOKEMON
			let expReward = 0;
			let goldReward = 0;
			if(isWinBattle){
				if(isWildPokemon){
					expReward = expeditionBattle.wildPokemon.reward.exp
				}else{ // Jika trainer maka loop tiap trainer pokemon
					expeditionBattle.trainer.pokemons.forEach(poke => {
						expReward += poke.reward.exp
						goldReward += poke.reward.gold
					})
				}	
			}
			// update history
			const historyId = user.expeditionBattle.historyId
			const expedition = user.expeditions.find(exp => exp._id.equals(historyId))
			expedition.moves = expeditionBattle.moves
			expedition.status = expeditionBattle.status
			// Update Pokemon
			const player = expeditionBattle.player
			const allBattlePokemon = [player.activePokemon, ...player.inactivePokemons]
			allBattlePokemon.forEach(battlePoke => {
				const pokemon = user.pokemons.find(poke => poke._id.equals(battlePoke._id))
				// Update Attribute
				pokemon.hp = battlePoke.hp
				pokemon.sp = battlePoke.sp
				// Update Exp
				if(isWinBattle){
					giveExpPokemon(pokemon, expReward)
				}
				console.log(pokemon)
			})
			// Update player money
			user.money += goldReward
			let reward = null
			if(isWinBattle){
				reward = {}
				reward.exp = expReward
				if(goldReward > 0){
					reward.gold = goldReward
				}
				expedition.reward = reward
			}

			// Jika player sudah melihat hasil akhir battle
			user.expeditionBattle = undefined
			user.status = UserStatus.IDLE
			user.markModified('expeditions')
			user.markModified('expeditionBattle')
			user.markModified('pokemons')
			await user.save()
			return res.status(200).json({
				msg: 'Battle ended!',
				reward: isWinBattle ? reward : undefined
			})
		}else{ // Selain endbattle inputan invalid
			return res.status(400).json({
				msg: 'Invalid action for ended game!'
			})
		}
	}
})

// GET /explore/history
router.get('/history', authUser, async (req, res) => {
	const { _user: user } = req.body
	const histories = []
	for(let i=0;i<user.expeditions.length;i++){
		const history = user.expeditions[i]
		const expedition = await Expedition.findById(history.expeditionId, {
			name: 1,
			description: 1,
			image: 1
		})
		histories.push({
			historyId: history._id,
			expedition,
			date: history.date,
			drop: history.drop,
		})
	}
	return res.status(200).json({
		histories
	})
})

// GET /explore/history/:id
router.get('/history/:id', authUser, async (req, res) => {
	const { _user: user } = req.body
	const { id: historyId } = req.params
		
	const history = user.expeditions.find(exp => exp._id == historyId)
	if(!history){ // jika not found
		return res.status(404).json({
			msg: 'History not found!'
		})
	}
	
	const expedition = await Expedition.findById(history.expeditionId, {
		name: 1,
		description: 1,
		image: 1
	})
	const retObj = {
		historyId: history._id,
		expedition,
		...history._doc,
		_id: undefined,
		expeditionId: undefined
	}
	return res.status(200).json(retObj)
})

function updateExploreRemaining(user){
	if(user.isPremium) return;
	user.remainingExplore--;
}

// Helper Function
function isPlayerLoseBattle(expeditionBattle){
	const isPlayerLose = [ // End ketika semua pokemon player mati
		expeditionBattle.player.activePokemon, 
		...expeditionBattle.player.inactivePokemons
	].every(poke => poke.hp<=0)
	return isPlayerLose
}
function isEnemyLosebattle(expeditionBattle){ // Masih Wild Pokemon
	// End ketika semua pokemon musuh mati
	if(expeditionBattle.expeditionType == ExpeditionType.TRAINER){
		const isAllPokeDead = expeditionBattle.trainer.pokemons.every(poke => poke.hp <= 0)
		return isAllPokeDead

	}else{
		const isEnemyLose = expeditionBattle.wildPokemon.hp <= 0
		return isEnemyLose
	}
}

function isGameOver(expeditionBattle){
	const isPlayerLose = isPlayerLoseBattle(expeditionBattle)
	const isEnemyLose = isEnemyLosebattle(expeditionBattle)
	return isPlayerLose || isEnemyLose
}

// Display Enemy
async function mapPokeEnemy(enemyPoke){
	const pokeTemplate = await PokedexPokemon.findOne({entryNo: enemyPoke.entryNo})
	const sprite = pokeTemplate.image
	let image = ''
	if(enemyPoke.gender == Gender.FEMALE){
		image = enemyPoke.isShiny ? sprite.shinyFemale : sprite.female;
	}else{
		image = enemyPoke.isShiny ? sprite.shinyMale : sprite.male;
	}
	return {
		level: enemyPoke.level,
		name: enemyPoke.name,
		image,
		isShiny: enemyPoke.isShiny,
		gender: enemyPoke.gender,
		types: enemyPoke.types,
		hp: enemyPoke.hp,
		maxHp: enemyPoke.maxHp,
	}
}

// Generate Wild Pokemon
async function generateWildExpeditionPokemon(expedition){
	const pokeTemplates = expedition.drop.wildPokemons
	const randomIdx = getRandomInt(0, pokeTemplates.length - 1)
	const pokeTemplate = pokeTemplates[randomIdx]

	const wildPokemon = await generatePokemon(pokeTemplate.entryNo, 0.1)
	wildPokemon.reward = { exp: pokeTemplate.exp }
	levelUpPokemon(wildPokemon, getRandomInt(pokeTemplate.level.min, pokeTemplate.level.max) - 1)

	return wildPokemon
}

// Generate Trainer
async function generateWildTrainer(expedition){
	const trainers = expedition.drop.trainers
	const idx = getRandomInt(0, trainers.length - 1)
	const trainerTemplate = await ExpeditionTrainer.findById(trainers[idx])

	const { min: minPokemon, max: maxPokemon} = trainerTemplate.pokemonBrought
	const nPokemon = getRandomInt(minPokemon, maxPokemon)
	const pokemons = []
	for(let i=0;i<nPokemon;i++){
		const idxPokemon = getRandomInt(0, trainerTemplate.pokemons.length-1)
		const entryNo = trainerTemplate.pokemons[idxPokemon]
		const newPokemon = await generatePokemon(entryNo, 0.01)
		const { min: minLevel, max: maxLevel } = trainerTemplate.levelRange
		const level = getRandomInt(minLevel, maxLevel)
		levelUpPokemon(newPokemon, level-1)
		newPokemon.reward = trainerTemplate.reward
		pokemons.push(newPokemon)
	}

	const newTrainer = {
		name: trainerTemplate.name,
		description: trainerTemplate.description,
		image: trainerTemplate.image,
		pokemons
	}
	return newTrainer
}

// Insert Trainer Expedition
async function insertWildTrainerExpedition(user, expeditionId, pokemons, trainer){
	updateExploreRemaining(user)
	user.expeditions.push({
		expeditionId,
		date: new Date(),
		drop: ExpeditionType.TRAINER,
		pokemons,
		trainer
	});
	const expedition = user.expeditions[user.expeditions.length - 1]
	return expedition
}

// Insert Wild Pokemon Expedition
async function insertWildPokeExpedition(user, expeditionId, pokemons, wildPokemon){
	updateExploreRemaining(user)
	user.expeditions.push({
		expeditionId,
		date: new Date(),
		drop: ExpeditionType.WILDPOKEMON,
		pokemons,
		wildPokemon
	});
	const expedition = user.expeditions[user.expeditions.length - 1]
	return expedition
}


module.exports = router
