const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')
const { authUser } = require('../middlewares')
const { Egg, Item } = require('../models')
const { generateEntryPokemon } = require("../modules/pokeFactory");
const { PokedexStatus } = require('../variables')


// Digunakan untuk mendaptakn semua telur yang dimiliki oleh user tersebut
router.get('/',authUser, async(req,res)=>{
    const { // destructuring
        _user: user
    } = req.body
    
    //menaruh kedalam suatu variabel
    const egg = {
        eggUser:user.eggs
    };
    
    //mereturnkan kepada user.
    res.status(200).send(egg);
}) 

//digunakan untuk melihat detail dari telur yang ada 
router.get('/:id',authUser, async(req,res)=>{
    const { // destructuring
        _user: user,
    } = req.body
    const id = req.params.id; //id telur yang ingin dilihat detailnya
    
    //mencarinya pada user apakah ada telur dengan id yang dimaksudkan;
    const indexegg = user.eggs.findIndex(eggs=> eggs._id.equals(id))
    console.log(indexegg)
    if (indexegg < 0 ) {
        return res.status(404).json({
            msg: "Egg not found"
        })
    }

    //untuk mendapatkan data detailnya karena tadi hanya berupa id saja;
    let detailegg =[await Egg.populate(user.eggs[indexegg], {
        path: 'eggId',
        select: '-_id -__v -images -shinyRate -exploreRequirement'
    })];
    
    //lalu saya lakukan map baru saya kembalikan ke user;
    detailegg = detailegg.map(function (telur) {
        return {
            _id:telur.id,
            egg:telur.eggId,
            image:telur.image,
            exploreRemaining:telur.exploreRemaining
        };
    });
    
    //saya pilih array ke 0 karena returnya udah pasti 1 telur
    const sendData = {
        detailEgg:detailegg[0]
    }

    return res.status(200).send(sendData)
}) 


//digunakan untuk menjual telur 
router.post('/sell',authUser, async(req,res)=>{
    const { // destructuring
        _user: user,
    } = req.body
    
    //id dari telur yang ingin dicari
    const id = req.body.idegg;
    
    //mencari aapakah ada 
    const indexegg = user.eggs.findIndex(eggs=> eggs._id.equals(id));

    if (indexegg < 0 ) {
        return res.status(404).json({
            msg: "Egg not found"
        })
    }

    //mendapatkan data detail egg yang sudah ditemukan tadi
    let detailegg =[await Egg.populate(user.eggs[indexegg], {
        path: 'eggId',
        select: '-_id -__v -images -shinyRate -exploreRequirement'
    })];
    

    //menambahkan uang user
    user.money += user.eggs[indexegg].eggId.price;
    //menghapus telur yang di jual dari user
    user.eggs = user.eggs.filter(function (egg) {
        return egg._id !=id
    });
    //melakukan save
    await user.save();
    

    //memberikan pesan kepada user
    return res.status(200).send( {
        msg: "Bye bye my beloved egg",
        money: user.money,
      })
}) 


// untuk
router.post('/hatch',authUser, async(req,res)=>{
  
    const { // destructuring
        _user: user,
    } = req.body
    
    //id dari telur yang ingin dicari
    let id = req.body.idegg;
    //mencari aapakah ada 
    const indexegg = user.eggs.findIndex(eggs=> eggs._id.equals(id));

    if (indexegg < 0 ) {
        return res.status(404).json({
            msg: "Egg not found"
        })
    }
    if (user.eggs[indexegg].exploreRemaining != 0) {
        return res.status(404).json({
            msg: "This egg doesn't ready to hatch"
        })
    }
    let detailegg =await Egg.populate(user.eggs[indexegg], {
        path: 'eggId',
        select: '-_id -__v -images -exploreRequirement'
    });
    
    const shinyrate = detailegg.eggId.shinyRate;
    let tempPokemon =await generateEntryPokemon(shinyrate);
    user.eggs = user.eggs.filter(function (egg) {
        return egg._id !=id
    });

    const item = await Item.findOne({name:"poke-ball"})
    tempPokemon.isActive=true
    tempPokemon.pokeball=item._id
    tempPokemon.maxExp=200
    tempPokemon.exp=0
    // const dictbaru

    const entryNo = tempPokemon.entryNo.toString();
    if(!user.pokedex.has(entryNo)){
        user.pokedex.set(entryNo,PokedexStatus.CAUGHT);
    }
    // return res.send(user.pokedex)
    // let temp[tempPokemon.entryNo]=""
    user.pokemons.push(tempPokemon);

    await user.save();
    
    let sendData = {
        msg :"Congratulations your egg hatched",
        pokemon:tempPokemon
    }
    return res.status(200).send(sendData)
}) 

router.get('/eggajaib',authUser,async(req,res)=>{
    const { // destructuring
        _user: user
    } = req.body

    // return res.send(user);
    let Allegg = await Egg.find({});
    user.eggs.push({
        eggId:Allegg[0]._id,
        image:Allegg[0].images[0],
        exploreRemaining:100
    });
    await user.save();
    return res.send(user)
    // return res.send(Allegg);

   
}) 
module.exports = router;