const express = require('express')
const { validateStatus, authUser } = require('../middlewares')
const { Multiplayer, User } = require('../models')
const { UserStatus, BattleAction, MultiplayerStatus } = require('../variables')
const router = express.Router()

// /battle
router.get('/', authUser, validateStatus(UserStatus.MULTIPLAYER), async (req, res) => {
	const { _user:user } = req.body
	let multiplayer = await Multiplayer.findById(user.multiplayer, {
		'_id': 0,
		'__v': 0
	})
	if(multiplayer.status == MultiplayerStatus.ONGOING){
		multiplayer = {...multiplayer._doc}
		const user1 = await User.findById(multiplayer.player1.userId)
		const user2 = await User.findById(multiplayer.player2.userId)
		multiplayer.player1.user = user1.username
		multiplayer.player1.userId = undefined
		multiplayer.player1.pokemons = undefined
		multiplayer.player1.action = undefined
		multiplayer.player2.user = user2.username
		multiplayer.player2.userId = undefined
		multiplayer.player2.pokemons = undefined
		multiplayer.player2.action = undefined
		return res.status(200).json(multiplayer)
	}else{ // Finish
		multiplayer = {...multiplayer._doc}
		let winner = await User.findById(multiplayer.winner)
		const user1 = await User.findById(multiplayer.player1.userId)
		const user2 = await User.findById(multiplayer.player2.userId)
		multiplayer.player1.user = user1.username
		multiplayer.player1.userId = undefined
		multiplayer.player2.user = user2.username
		multiplayer.player2.userId = undefined
		multiplayer.winner = winner.username

		return res.status(200).json(multiplayer)
	}
})
router.post('/', authUser, validateStatus(UserStatus.MULTIPLAYER), async (req, res) => {
	let { 
		action, // Dalam angka
		_user:user 
	} = req.body
	action = parseInt(action)
	// Check if action valid
	const possibleAction = [
		BattleAction.ATTACK,
		BattleAction.SPECIAL,
		BattleAction.RUN,
		BattleAction.ENDBATTLE
	];
	if(!possibleAction.includes(action)){
		return res.status(400).json({
			msg: 'Invalid action!'
		})
	}
    // Get Room
    const multiplayer = await Multiplayer.findById(user.multiplayer)
	let player = null;
	let opponent = null;
	if(multiplayer.player1.userId.equals(user._id)){
		player = multiplayer.player1
		opponent = multiplayer.player2
	}else{
		player = multiplayer.player2
		opponent = multiplayer.player1
	}
	// Do Action
	if(multiplayer.status == MultiplayerStatus.ONGOING){ // Jika masih berjalan
		let actPoke = player.activePokemon
		let oppPoke = opponent.activePokemon

		// Untuk Create Move Log
		function createLog(log){
			const move = {
				index: multiplayer.moves.length+1,
				log
			}
			multiplayer.moves.push(move)
			console.log(move)
		}

		// Jika surender
		if(action == BattleAction.RUN){
			endMultiplayer(multiplayer, opponent.userId)
			const playerObj = await User.findById(player.userId)
			createLog(`${playerObj.username} surrender!`)
			await multiplayer.save()
			return res.status(200).json({
				msg: 'successfully perform action!',
				action: BattleAction.toString(action),
			}) 
		}

		// Jika player sudah mengambil action
		if(player.action != null){ // jika sebelumnya sudah pilih action
			return res.status(400).json({
				msg: 'Player already choose action!'
			})
		}

		// Validate End Battle
		if(action == BattleAction.ENDBATTLE){ // Tidak valid
			return res.status(400).json({
				msg: 'Invalid move for non-ended battle!'
			})
		}

		// Validate Special Attack
		if(action == BattleAction.SPECIAL && actPoke.sp <= 0){
			return res.status(400).json({
				msg: 'Pokemon doesn\'t have enough sp!'
			})
		}

		// Move valid maka simpan ke multiplayer
		player.action = action;
		multiplayer.markModified('player1')
		multiplayer.markModified('player2')
		await multiplayer.save()
		
		const isBothMove = player.action != null && opponent.action != null
		if(!isBothMove){ // Jika menunggu action dari lawan
			return res.status(200).json({
				msg: 'successfully set action, waiting for opponent action!',
				action: BattleAction.toString(action),
			}) 
		}else{ // Jika keduanya sudah bergerak
			const isPlayerMoveFirst = actPoke.spd >= oppPoke.spd;
			async function playerAction(player1, player2){
				// Check End Battle
				if(multiplayer.status != MultiplayerStatus.ONGOING) return;
				
				const poke1 = player1.activePokemon
				const poke2 = player2.activePokemon
				if(poke1.hp <= 0) return;
				console.log('Poke1: ' + player1.activePokemon.name)
				const choosenAction = player1.action
				let dmg = 0
				if(choosenAction == BattleAction.ATTACK){
					dmg = poke1.atk - poke2.def
					createLog(`${poke1.name} use basic attack!`)
				}else if(choosenAction == BattleAction.SPECIAL){
					dmg = poke1.spAtk + poke1.atk - poke2.spDef
					poke1.sp--
					createLog(`${poke1.name} use special attack!`)
				} 
				poke2.hp -= dmg
				dmg = Math.max(1, dmg)
				createLog(`${poke2.name} took ${dmg} damage!`)
	
				// Check darah musuh
				if(poke2.hp <= 0){
					createLog(`${poke2.name} fainted!`)
				}
			}

			async function trySwitchPokemon(trainer, opponent){
				if(trainer.activePokemon.hp <= 0){ // Jika main pokemon darahnya habis
					const trainerobj = await User.findById(trainer.userId)
					if(isPlayerLose(trainer)){ // Jika sudah kalah
						createLog(`${trainerobj.username} defeated!`)
						endMultiplayer(multiplayer, opponent.userId)
					}else{ // Jika blm kalah maka switch pokemon
						const curPoke = trainer.activePokemon
						const idxNewPoke = trainer.inactivePokemons.find(poke => poke.hp > 0)
						const [newPoke] = trainer.inactivePokemons.splice(idxNewPoke,1)
						trainer.inactivePokemons.push(curPoke)
						trainer.activePokemon = newPoke
						createLog(`${trainerobj.username} choose ${newPoke.name}!`)
					}
				}
			}
			
			// Move First
			if(isPlayerMoveFirst){
				await playerAction(player, opponent)
				await playerAction(opponent, player)
			}else{
				await playerAction(opponent, player)
				await playerAction(player, opponent)
			}
			player.action = null
			opponent.action = null
			// Switch pokemon kalau mati
			await trySwitchPokemon(player, opponent);
			await trySwitchPokemon(opponent, player);
			// Save Multiplayer
			multiplayer.markModified('player1')
			multiplayer.markModified('player2')
			await multiplayer.save()

			if(multiplayer.status == MultiplayerStatus.FINISH){
				// Unset player1 & player2
				await Multiplayer.updateMany({_id: multiplayer._id}, {
					$unset: {
						'player1.activePokemon': '',
						'player1.inactivePokemons': '',
						'player1.action': '',
						'player2.activePokemon': '',
						'player2.inactivePokemons': '',
						'player2.action': '',
					}
				})
			}

			return res.status(200).json({
				msg: 'successfully perform action!',
				action: BattleAction.toString(action),
			}) 
		}
	}else{ // Jika sudah berakhir
		if(action != BattleAction.ENDBATTLE){
			return res.status(400).json({
				msg: 'Invalid action for ended game!'
			})
		}
		// jika ranked maka check pemenang
		if(multiplayer.isRanked){
			const isWinning = multiplayer.winner == user._id+''
			if(isWinning){
				user.money += 2000;
			}
		}
		// Update User Status
		user.status = UserStatus.IDLE
		user.multiplayer = undefined
		await user.save()
		return res.status(200).json({
			msg: 'Battle ended!',
		})
	}
})

// /battle/history
router.get('/history', authUser, async(req,res)=>{
	const {
		_user: user
	} = req.body
	const multiplayers = []
	for(let i=0;i<user.multiplayers.length;i++){
		let multiplayer = await Multiplayer.findById(user.multiplayers[i])
		if(multiplayer.status != MultiplayerStatus.FINISH) continue;
		multiplayer = {
			...multiplayer._doc,
			__v: undefined,
			status: undefined
		}
		let winner = await User.findById(multiplayer.winner)
		const user1 = await User.findById(multiplayer.player1.userId)
		const user2 = await User.findById(multiplayer.player2.userId)
		multiplayer.player1.user = user1.username
		multiplayer.player1.userId = undefined
		multiplayer.player2.user = user2.username
		multiplayer.player2.userId = undefined
		multiplayer.winner = winner.username
		multiplayers.push(multiplayer)
	}
	res.status(200).json({
		multiplayers
	})
})
// /battle/history/:id
router.get('/history/:id', authUser, async(req,res)=>{
	const {
		_user: user
	} = req.body
	const id = req.params.id
	const multiplayerId = user.multiplayers.find(mult => mult == id)
	console.log(multiplayerId)
	if(multiplayerId == null){
		return res.status(404).json({
			msg: 'Multiplayer not found!'
		})
	}
	let multiplayer = await Multiplayer.findById(multiplayerId)
	if(multiplayer.status != MultiplayerStatus.FINISH){
		return res.status(404).json({
			msg: 'Multiplayer not found!'
		})
	}
	multiplayer = {
		...multiplayer._doc,
		__v: undefined,
		status: undefined
	}
	let winner = await User.findById(multiplayer.winner)
	const user1 = await User.findById(multiplayer.player1.userId)
	const user2 = await User.findById(multiplayer.player2.userId)
	multiplayer.player1.user = user1.username
	multiplayer.player1.userId = undefined
	multiplayer.player2.user = user2.username
	multiplayer.player2.userId = undefined
	multiplayer.winner = winner.username
	res.status(200).json(multiplayer)
})

// Helper function
function isPlayerLose(player){
	const isPlayerLose = [ // End ketika semua pokemon player mati
		player.activePokemon, 
		...player.inactivePokemons
	].every(poke => poke.hp<=0)
	return isPlayerLose
}

function endMultiplayer(multiplayer, winner){
	multiplayer.winner = winner
	multiplayer.status = MultiplayerStatus.FINISH
}

module.exports = router
