const express = require('express')
const { authUser, validateStatus } = require('../middlewares')
const { User, Room, Multiplayer } = require('../models')
const { validateBattlePokemon } = require('../modules/battle')
const { UserStatus, MultiplayerStatus } = require('../variables')
const router = express.Router()

require('dotenv').config()

// /multiplayer/room
// Get semua multiplayer room
router.get('/room', async(req, res)=>{
    const pubRooms = await Room.find({ isPublic: true }, {
        hostPokemons: 0,
        guestPokemons: 0,
        password: 0,
        _id: 0,
        __v: 0,
    })
    const rooms = []
    for(let i=0;i<pubRooms.length;i++){
        const pubRoom = pubRooms[i];
        if(pubRoom.guestId != null) continue;
        const host = await User.findById(pubRoom.hostId);
        rooms.push({
            ...pubRoom._doc,
            host: host.username,
            hostId: undefined,
            guestId: undefined,
        })
    }
    return res.status(200).json({ rooms })
})

// /multiplayer/room
// Buat Multiplayer room
router.post('/room', authUser, validateStatus(UserStatus.IDLE), async(req,res)=>{
    let {
        name,
        password,
        isPublic,
        _user: user
    } = req.body
    // Validate input
    const isNameValid = name != null && name != ""
    if(!isNameValid){ // If is invalid input
        return res.status(400).json({
            msg: 'Missing name field!'
        })
    }
    const isPubValid = isPublic === '0' || isPublic === '1'
    if(!isPubValid){
        return res.status(400).json({
            msg: 'Invalid isPublic field!'
        })
    }
    // Refine Input
    password = password || ""
    isPublic = isPublic === '1'
    // Generate Code
    let code = ''
    while(code == ''){ // generate unique room code
        const listChar = 'abcdefghijklmnopqrstuvxyz'
        code = new Array(5).fill(1).map(_ => listChar[Math.floor(Math.random()*listChar.length)]).join('')
        code = code.toUpperCase()
        // Check apakah dont exist
        const room = await Room.findOne({ code })
        if(room != null) code = ''
    }
    // Create Room
    const newRoom = new Room({
        name,
        code,
        password,
        isPublic,
        hostId: user._id
    })
    // Save Room
    await newRoom.save()
    // Update User Status
    user.status = UserStatus.ROOM
    user.roomId = newRoom._id
    await user.save()

    return res.status(201).json({
        name,
        code,
        isPublic,
        host: user.username
    })
})

// /multiplayer/join
router.post('/room/join', authUser, validateStatus(UserStatus.IDLE), async(req,res)=>{
    let {
        roomCode: code,
        password,
        _user: user
    } = req.body
    // Validate input
    const isCodeValid = code != ''
    if(!isCodeValid){ // If is invalid input
        return res.status(400).json({
            msg: 'Missing roomCode field!'
        })
    }
    // Refine Input
    password = password || ""
    // Find Room
    const room = await Room.findOne({code}).populate('hostId')
    if(room == null){
        return res.status(404).json({
            msg: 'Room not found!'
        })
    }
    // Check Room kosong
    if(room.guestId != null){
        return res.status(400).json({
            msg: 'Room is full!'
        })
    }
    // Check Password
    if(password != room.password){
        return res.status(400).json({
            msg: 'Password incorrect!'
        })
    }
    // Update Room
    room.guestId = user._id
    room.guestPokemons = []
    await room.save() 
    // Update Status User
    user.status = UserStatus.ROOM
    user.roomId = room._id
    await user.save()

    return res.status(201).json({
        name: room.name,
        code: room.code,
        isPublic: room.isPublic,
        host: room.hostId.username
    })
})

// /multiplayer/room/leave
router.post('/room/leave', authUser, validateStatus(UserStatus.ROOM), async(req,res)=>{
    const {
        _user: user
    } = req.body
    // Find Room
    const room = await Room.findById(user.roomId).populate('hostId')
    if(user.equals(room.hostId._id)){ // Jika Host
        const guest = await User.findById(room.guestId)
        // Room
        await room.remove()
        console.log('Room removed!')
        // Guest
        guest.roomId = undefined
        guest.status = UserStatus.IDLE
        await guest.save()
        console.log('Guest removed!')
        // Host
        user.roomId = undefined
        user.status = UserStatus.IDLE
        await user.save()
    }else{ // Jika Guest
        // Hpus Guest ID
        room.guestId = undefined
        await room.save()
        // Update 
        user.roomId = undefined
        user.status = UserStatus.IDLE
        await user.save()
    }
    res.status(200).json({
        name: room.name,
        code: room.code,
        isPublic: room.isPublic,
        host: room.hostId.username
    })
})

// /multiplayer/room/set-team
router.post('/room/set-team', authUser, validateStatus(UserStatus.ROOM), async(req,res)=>{
    const {
        _user: user,
        pokemons
    } = req.body
    // Validate Pokemon
    const isPokemonvalid = validateBattlePokemon(res, user, pokemons)
	if(!isPokemonvalid) return;
    // Jika valid maka update room
    const room = await Room.findById(user.roomId);
    const isHost = user.equals(room.hostId._id);
    if(isHost){
        room.hostPokemons = pokemons
    }else{
        room.guestPokemons = pokemons
    }
    await room.save()
    return res.status(200).json({
        msg: 'Successfully set team!'
    })
})

// /multiplayer/room/start
router.post('/room/start', authUser, validateStatus(UserStatus.ROOM), async(req,res)=>{
    const {
        _user: user,
    } = req.body
    const room = await Room.findById(user.roomId);
    const isHost = user.equals(room.hostId._id);
    if(!isHost){
        return res.status(400).json({
            msg: `User not room host!`
        })
    }
    // Check ready pokemon
    const hostPokeNotValid = room.hostPokemons.length <= 0;
    const guestNotFound = room.guestId == null
    const guestPokeNotValid = room.guestPokemons == null || room.guestPokemons.length <= 0
    const isValid = !hostPokeNotValid && !guestNotFound && !guestPokeNotValid
    if(!isValid){
        return res.status(400).json({
            msg: `Not all player ready!`
        })
    }
    const host = await User.findById(room.hostId)
    const guest = await User.findById(room.guestId)
    const player1 = new Player(host, room.hostPokemons)
    const player2 = new Player(guest, room.guestPokemons)
    const multiplayer = new Multiplayer()
    multiplayer.date = new Date()
    multiplayer.player1 = player1
    multiplayer.player2 = player2
    multiplayer.isRanked = false
	multiplayer.status = MultiplayerStatus.ONGOING
	multiplayer.moves = []
    await multiplayer.save()
    // Simpan multiplayer ke masing" history user + update status
    host.multiplayers.push(multiplayer._id)
    guest.multiplayers.push(multiplayer._id)
    host.status = guest.status = UserStatus.MULTIPLAYER
    host.roomId = guest.roomId = undefined
    host.multiplayer = guest.multiplayer = multiplayer._id
    await host.save()
    await guest.save()
    // Delete room
    await room.remove()

    return res.status(200).json({
        msg: 'Battle started!'
    })
})

// /multiplayer/match
router.post('/match', authUser, validateStatus(UserStatus.IDLE), async(req,res)=>{
    const {
        _user: user,
        pokemons
    } = req.body
    // Check Uang user
    if(user.money < 1000){
        return res.status(400).json({
            msg: 'User dont have enough money!'
        })
    }
    // Check Pokemon
    const isPokemonvalid = validateBattlePokemon(res, user, pokemons)
	if(!isPokemonvalid) return;
    // Buat Object Trainer
    const trainer = new Player(user, pokemons)
    // Coba cari ruang multiplayer yang player2nya kosong
    const mult = await Multiplayer.findOne({ isRanked: true, status: MultiplayerStatus.WAITING })
    const isMultFound = mult != null
    if(isMultFound){
        mult.player2 = trainer
        mult.status = MultiplayerStatus.ONGOING
        await mult.save()
        // Update User
        user.multiplayers.push(mult._id)
        user.status = UserStatus.MULTIPLAYER
        user.multiplayer = mult._id
        user.money -= 1000
        await user.save()
        // Update lawan
        const opponent = await User.findById(mult.player1.userId)
        opponent.status = UserStatus.MULTIPLAYER
        await opponent.save()

        return res.status(200).json({
            msg: 'Matching...',
            money: user.money
        })
    }else{ // jika tidak ketemu ruangan maka buat ruangan
        const multiplayer = new Multiplayer()
        multiplayer.date = new Date()
        multiplayer.player1 = trainer
        multiplayer.player2 = trainer
        multiplayer.isRanked = true
        multiplayer.status = MultiplayerStatus.WAITING
        multiplayer.moves = []
        await multiplayer.save()
        // Update User
        user.multiplayers.push(multiplayer._id)
        user.status = UserStatus.MATCHING
        user.multiplayer = multiplayer._id
        user.money -= 1000
        await user.save()

        return res.status(200).json({
            msg: 'Matching...',
            money: user.money
        })
    }
})

// /multiplayer/status
router.get('/status', authUser, async(req,res)=>{
    const {
        _user: user
    } = req.body
    if(user.status == UserStatus.ROOM){
        const room = await Room.findById(user.roomId, {
            hostPokemons: 0,
            guestPokemons: 0,
            password: 0,
            _id: 0,
            __v: 0,
        })
        let guest = null
        const thereIsGuest = room.guestId != null
        if(thereIsGuest){
            guest = await User.findById(room.guestId)
        }
        return res.status(200).json({
            ...room._doc,
            host: user.username,
            guest: thereIsGuest ? guest.username : undefined,
            hostId: undefined,
            guestId: undefined,
        })
    }else if(user.status == UserStatus.MATCHING){
        return res.status(200).json({
            msg: 'User is matching!'
        })
    }else{ // Invalid status
        return res.status(400).json({
            msg: 'User currently no in room or matching!'
        })
    }
})

// Helper Function
function Player(user, pokemons){
    this.userId = user._id
    pokemons = user.pokemons.filter(poke => pokemons.includes(poke._id+''))
    this.pokemons = pokemons
    // Battle
    this.activePokemon = pokemons[0]
    this.inactivePokemons = pokemons.slice(1)
    this.action = null
}

module.exports = router
