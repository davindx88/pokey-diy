const express = require("express");
const router = express.Router();
const axios = require('axios')
var jwt = require("jsonwebtoken");
const { authUser, validateStatus } = require("../middlewares");
const { Item, PokedexPokemon, PokeEvolve } = require("../models");
const { maxHp } = require("../models/types/pokemon");
const PokeFactory = require("../modules/pokeFactory");
const { UserStatus, ItemType, PokeAttribute } = require("../variables");

//post
router.post("/:id/use-item",
	authUser,
	validateStatus(UserStatus.IDLE),
	async (req, res) => {
		let pokemon;
		var id = req.params.id;
		var itemid = req.body.itemid;
		let userItem = req.body._user.items;
		let pokemonUser = req.body._user.pokemons;
		pokemonUser.forEach((element) => {
			if (element._id == id) {
				pokemon = element;
			}
		});
		let item = await Item.findOne({ _id: itemid });
		userItem.forEach((element) => {
			if (element.itemId.equals(item._id)) {
				if (element.qty > 0) {					
					if (item.type == ItemType.NUTRIENT) {
						if (item.attribute == PokeAttribute.ATK) {
							pokemon.atk += item.amount;
						} else if (item.attribute == PokeAttribute.DEF) {
							pokemon.def += item.amount;
						} else if (item.attribute == PokeAttribute.MAXHP) {
							pokemon.maxHp += item.amount;
						} else if (item.attribute == PokeAttribute.LEVEL) {
							PokeFactory.levelUpPokemon(pokemon, 1);
						} else if (item.attribute == PokeAttribute.MAXSP) {
							pokemon.maxSp += item.amount;
						} else if (item.attribute == PokeAttribute.SPATK) {
							pokemon.spAtk += item.amount;
						} else if (item.attribute == PokeAttribute.SPDEF) {
							pokemon.spDef += item.amount;
						} else if (item.attribute == PokeAttribute.SPD) {
							pokemon.spd += item.amount;
						}
					} else if (item.type == ItemType.POTION) {
						if (item.isPercentage) {
							pokemon.hp += (item.amount / 100 * pokemon.maxHp)
						} else {
							pokemon.hp += item.amount
						}
						if (pokemon.hp > pokemon.maxHp) {
							pokemon.hp = pokemon.maxHp
						}
					} else if (item.type == ItemType.REVIVE) {
						if (pokemon.hp == 0) {
							if (item.isPercentage) {
								pokemon.hp += (item.amount / 100 * pokemon.maxHp)
							} else {
								pokemon.hp += item.amount
							}
							if (pokemon.hp > pokemon.maxHp) {
								pokemon.hp = pokemon.maxHp
							}
						} else {
							res.send("Cant use revive on alive pokemon").status(403)
						}
					} else if (item.type == ItemType.ETHER) {
						if (item.isPercentage) {
							pokemon.sp += (item.amount / 100 * pokemon.maxSP)
						} else {
							pokemon.sp += item.amount
						}
						if (pokemon.sp > pokemon.maxSp) {
							pokemon.sp = pokemon.maxSp
						}
					}
					element.qty -= 1
					if (element.qty == 0) {
						userItem.deleteOne({ _id: element.itemId })
					}
				}
			}
		});
		await req.body._user.save();
		res.send({ pokemon });
	}
);
router.post("/:id/sell",
	authUser,
	validateStatus(UserStatus.IDLE),
	async (req, res) => {
		let pokemon;
		var id = req.params.id;
		let pokemonUser = req.body._user.pokemons;
		let user = req.body._user;
		console.log(pokemonUser);
		pokemonUser.forEach((element) => {
			if (element._id == id) {
				pokemon = element;
			}
		});
		if (pokemon.isActive == false) {
			res.send("Pokemon not available").status(403);
		} else {
			if(pokemonUser.size>1){
				user.money += pokemon.level * 500;
				pokemon.isActive = false;
				await req.body._user.save();
				let returnedUser = user.pokemons
				res.send(returnedUser).status(200);
			}else{
				res.send("You cant sell your one and only pokemon")
			}
		}
	}
);
router.post("/:id/evolve",
	authUser,
	validateStatus(UserStatus.IDLE),
	async (req, res) => {
		var id = req.params.id;
		let pokemon
		let user = req.body._user
		let pokemonUser = user.pokemons;
		pokemonUser.forEach((element) => {
			if (element._id == id) {
				pokemon = element;
			}
		});
		let userItem = user.items;
		var evolutionTo = req.body.nextID
		const pokedexPoke = await PokedexPokemon.findOne({ entryNo:pokemon.entryNo })
		const evolveNode = await PokeEvolve.findOne({ pokemon: pokedexPoke._id })
		let evolution
		for (let i = 0; i < evolveNode.evolveTo.length; i++) {
			const element = evolveNode.evolveTo[i];
			evolution =await PokeEvolve.findById(element._id)
			if(evolution._id==evolutionTo){
				break
			}
		}
		let requirement= evolution.requirements
		let temp = []
		let item = await Item.findById('60ba0c8729b6091ddc452435')
		console.log(item.elementType)
		requirement.forEach(element => {
			userItem.forEach(elementUser => {
				console.log(elementUser)
				if(elementUser.itemId.equals(item._id)){
					if(item.elementType==element.element){
						if(elementUser.qty>=element.amount){
							temp.push(true)
							elementUser.qty-=element.amount
						}else{
							res.send("you need more stone")
						}
					}
				}
			});
		});
		if(temp.length>0){
			if(requirement.length==2){
				if(temp[0]==true && temp[1]==true){
					var nextevo = await PokedexPokemon.findOne({ _id:evolution.pokemon })
					pokemon.entryNo=nextevo.entryNo
					pokemon.types=nextevo.types
					
				}
			}else if(requirement.length==1){
				if(temp[0]==true){
					var nextevo = await PokedexPokemon.findOne({ _id:evolution.pokemon })
					pokemon.entryNo=nextevo.entryNo
					pokemon.types=nextevo.types
				}
			}else{
				res.send("All requirement must be reached")
			}
			await user.save()
			res.send({pokemon})
		}else{
			res.send("All requirement must be reached")
		}
	}
);

//get
router.get("/", authUser, async (req, res) => {
	let pokemonUser = req.body._user.pokemons;
	let pokemons= pokemonUser
	res.send({pokemons});
});
router.get("/:id", authUser, async (req, res) => {
	var pokemon;
	var id = req.params.id;
	let pokemonUser = req.body._user.pokemons;
	console.log(pokemonUser);
	pokemonUser.forEach((element) => {
		if (element._id == id) {
			pokemon = element;
		}
	});
	if (pokemon != null) {
		res.send({pokemon});
	} else {
		res.send("No Pokemon Found").status(301);
	}
});

//put
router.put(
	"/:id",
	authUser,
	validateStatus(UserStatus.IDLE),
	async (req, res) => {
		let pokemon;
		var id = req.params.id;
		let name = req.body.name;
		let pokemonUser = req.body._user.pokemons;
		console.log(pokemonUser);
		//pokemon = pokemonUser.find(el => el._id === id)
		pokemonUser.forEach((element) => {
			if (element._id == id) {
				pokemon = element;
			}
		});
		if (pokemon != null) {
			pokemon.name = name;
			await req.body._user.save();
			console.log("==============");
			console.log(pokemon);
			res.send(pokemon);
		} else {
			res.send("No Pokemon Found").status(301);
		}
	}
);

module.exports = router;
