const express = require('express')
const router = express.Router()
const { authUser } = require('../middlewares')
const { Item } = require('../models')

router.get('/',authUser, async(req,res)=>{
    const { // destructuring
        _user: user
    } = req.body
    
    let userItem =await Item.populate(user.items, {
        path: 'itemId',
        select: '-__v'
    });
    
    userItem = userItem.map(function (el) {
        return {
            _id: el.itemId._id,
            item: {
                ...el.itemId._doc,
                _id: undefined
            },
            qty: el.qty
        };
    });

    const senditem = {
        items:userItem
    };
 
    res.status(200).send(senditem);
})  

module.exports = router
