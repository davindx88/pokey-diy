const express = require("express");

const { authUser } = require("../middlewares");
const { User, Item } = require("../models");
const { generatePokemon } = require("../modules/pokeFactory");
const { UserStatus } = require("../variables")
const jwt = require("jsonwebtoken");
const moment = require("moment");
require("dotenv").config()
const router = express.Router();
//post
router.post("/register", async (req, res) => {
	let username = req.body.username;
	let password = req.body.password;
	let checkUser = User.find({ username: username });
	let starter = parseInt(req.body.choose);
	let startPokemon;
	if (starter == 1) {
		startPokemon = await generatePokemon(1);
	} else if (starter == 2) {
		startPokemon = await generatePokemon(4);
	} else {
		startPokemon = await generatePokemon(7);
	}
	const item = await Item.findOne({ name: "poke-ball" })
	startPokemon.isActive = true
	startPokemon.pokeball = item._id
	startPokemon.maxExp = 200
	startPokemon.exp = 0
	if ((await checkUser).length > 0) {
		res.send("Username already exist").status(400);
	} else {
		let newUser = new User({
			username,
			password,
			image: "aaaaaa",
			money: 500,
			isPremium: false,
			exploreCount: 0,
			remainingExplore: 5,
			multiplayerCount: 0,
			winrate: 0,
			startingDate: Date.now(),
			pokemons: [
				startPokemon,

			],
			items: [],
			eggs: [],
			pokedex: {},
			expeditions: [],
			multiplayers: [],
			lastLogin: Date.now(),
			status: UserStatus.IDLE,
		});
		await newUser.save();
		res.send(newUser).status(200);
	}
});
router.post("/login", async (req, res) => {
	let username = req.body.username;
	let password = req.body.password;
	let login = await User.findOne({ username, password })
	console.log(login)
	if (login != null) {
		const expiredObj = { expiresIn: '2h' }
		var token = jwt.sign({ username: username }, process.env.JWT_SECRET, expiredObj);
		// Update Last Login + Reset Daily Exploration
		const lastDate = login.lastLogin
		const currDate = new Date()
		login.lastLogin = currDate
		if(moment(lastDate).format('DD/MM/YYYY') != moment(currDate).format('DD/MM/YYYY')){
			// Jika hari maka reset daily explore
			login.remainingExplore = 5
		}
		await login.save()
		console.log(token);
		return res.status(200).json({
			msg: 'Successfully login!',
			token
		})
	} else {
		return res.status(400).json({
			msg: 'incorrect username or password!'
		})
	}
});
router.post("/upgrade", authUser, async (req, res) => {
	let user = req.body._user
	if(user.isPremium){ // Jika sudah premium
		return res.status(400).json({
			msg: 'User already premium user!'
		})
	}
	user.isPremium = true
	await user.save()
	res.status(200).json({
		msg: 'Successfully upgrade user!'
	})
});
//get
router.get("/", authUser, async (req, res) => {
	let user = req.body._user
	user = {
		...user._doc,
		password: undefined,
		_id: undefined,
		__v: undefined
	}
	res.status(200).json(user)
});
router.post('/topup', authUser, async (req, res) => {
	const { _user: user } = req.body
	user.money += 100000
	await user.save()
	res.status(200).json({
		msg: 'Topup success!',
		money: user.money
	})
})

module.exports = router;
