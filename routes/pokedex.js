const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')
const { authUser } = require('../middlewares')
const { PokedexStatus } = require('../variables')
const axios = require('axios')
const { PokedexPokemon, PokeEvolve } = require('../models')

router.get('/',authUser, async(req,res)=>{
    const { // destructuring
        _user: user
    } = req.body
    
    const offset = req.query.offset;// mulai daari berapa index berapa pokemon yang ingin kita lihat (+1)
    const limit = req.query.limit;//seberapa banyak

    const urlpokedex = "https://pokeapi.co/api/v2/pokemon?offset="+offset+"&limit=" + limit;//link untuk memanggil third api 
    
    let {data:{results:pokeapiPokemon}} = await axios.get(urlpokedex);//memanggil third api

    pokeapiPokemon = await Promise.all(  pokeapiPokemon.map( async (paPokemon) => {
        let entryNo = paPokemon.url;
        //proses mendapatkan index pokemon semisal charmender indexnya 1
        entryNo = entryNo.substr(("https://pokeapi.co/api/v2/pokemon/").length)
        entryNo = entryNo.substr(0, entryNo.indexOf("/")); 
        entryNo = parseInt(entryNo) 

        const status = getStatusPokedex(entryNo, user.pokedex)//mendapatkan status apakah dia ada di pokedex atau tidak
        if (!checkHasPokedex(status)) {
            return { 
                entryNo,
                status
            }
        }
        else{
            const project = {
                _id: 0,
                __v: 0,
                entryNo: 0,
                atk: 0,
                def: 0,
                spAtk: 0,
                spDef: 0,
                spd: 0,
                maxHp: 0,
                maxSp: 0,
            }
            let pokemon = await PokedexPokemon.findOne({entryNo}, project)
            return {
                entryNo,
                status,
                pokemon
            };
        }
    }));
    return res.status(200).send({allPokemon:pokeapiPokemon});
}) 

router.get('/:id',authUser, async(req,res)=>{
    const { // destructuring
        _user: user,
    } = req.body
    let entryNo = req.params.id;

    // Pengecekan status
    entryNo = parseInt(entryNo)
    const status = getStatusPokedex(entryNo, user.pokedex)
    let returndata = null
    if(checkHasPokedex(status)){
        // Dapetin Detail Pokemon
        const project = {
            __v: 0,
            entryNo: 0,
            atk: 0,
            def: 0,
            spAtk: 0,
            spDef: 0,
            spd: 0,
            maxHp: 0,
            maxSp: 0,
        }
        let pokemon = await PokedexPokemon.findOne({entryNo}, project)
   
        // Dapetin Evolve Tree
        let nodeEvolve = await PokeEvolve.findOne({pokemon:pokemon._id});    
        let rootEvolve = await GetevolveFromTree(nodeEvolve._id);
        rootEvolve = await GetevolveToTree(rootEvolve._id, user.pokedex);

        const evolveTree = rootEvolve
        pokemon={
            "types": pokemon.types,
            "name": pokemon.name,
            "image": pokemon.image,
            "height": pokemon.height,
            "weight": pokemon.weight
        };
        returndata = {
            entryNo,
            status,
            pokemon,
            evolveTree
        }
    }else{ // Jika tidak pernah ditanggkap atau dilihat
        returndata = {
            entryNo,
            status
        }
    }
    return res.status(200).send({
        detailPokemon:returndata
    });
}) 

async function GetevolveFromTree(_id) { // Dapetin Root
    let tempTree = await PokeEvolve.findOne({_id});

    while(tempTree.evolveFrom != null){
        tempTree = await GetevolveFromTree(tempTree.evolveFrom);
    }
    return tempTree;
}   

async function GetevolveToTree(idEvolve, pokedex) {
    let node = await PokeEvolve.findById(idEvolve);
    node = {...node._doc};
    node = await PokedexPokemon.populate(node,{
        path:"pokemon",
        select:"types name entryNo image"
    });
    node.__v=undefined;
    node.evolveFrom=undefined;
    node.requirements = node.requirements.map( ({element,amount}) => ({element,amount}) );

    // const entryNo = node.pokemon._id
    // const status = getStatusPokedex(node._id, pokedex)

    let {evolveTo} = node;
    if (evolveTo.length > 0) {
        for (let i = 0; i < evolveTo.length; i++) {
            evolveTo[i] = await PokeEvolve.findById(evolveTo[i]); 
            const evolveChild = evolveTo[i];
            evolveTo[i] = await GetevolveToTree(evolveChild, pokedex);
        }
    }
    return node;
}

function getStatusPokedex(entryNo, pokedex){
    entryNo += ""
    const status = !pokedex.has(entryNo) ? PokedexStatus.NOTFOUND : pokedex.get(entryNo)
    return status
}

function checkHasPokedex(status){
    return status !== PokedexStatus.NOTFOUND
}

module.exports = router
