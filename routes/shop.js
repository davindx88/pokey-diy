const express = require("express");
const { ShopItem, Item } = require("../models");
const { authUser, validateStatus } = require("../middlewares");
const { UserStatus } = require("../variables");

const router = express.Router();

// GET /shop
router.get("/", authUser, validateStatus(UserStatus.IDLE), async (req, res) => {
  let {
    // destructuring
    _user: user,
  } = req.body;

  //untuk mendapatkan item yang dijual pada shop(hanya beriisi id itemnya saja belum ada diteilnya)
  let items = await ShopItem.aggregate()
    .addFields({ item: "$itemId" })
    .project({ __v: 0 });
  
  //untuk mendapatkan detail item yang dijual pada shop
  await Item.populate(items, {
    path: "item",
    select: "-_id -__v",
  });

  // untuk melakukan map pada hasil agar lebih tersturuktur;
  items = items.map(function (elementItem) {
    let index = user.items.findIndex((item) =>
      item.itemId.equals(elementItem.itemId)
    );
    elementItem.ownedQty = index >=0 ? user.items[index].qty : 0;
    elementItem.itemId = undefined
    return elementItem
  });
  // return res.send(items)
  return res.status(200).json({
    shop:items,
  });
});


// POST /buy
router.post(
  "/buy",
  authUser,
  validateStatus(UserStatus.IDLE),
  async (req, res) => {
    let {
      // destructuring
      shopItemId,
      qty,
      _user: user,
    } = req.body;
    // Check jika Inputan tidak valid
    if (shopItemId == null) {
      return res.status(400).json({
        msg: "missing item id",
      });
    }

    if (!qty || !qty.match("^[0-9]*$")) {
      return res.status(400).json({
        msg: "item quantity is not valid",
      });
    }
    qty = parseInt(qty);

    // Check apakah item ada
    let itemShop = null
    try{
      itemShop = await ShopItem.findById(shopItemId);
      if(!itemShop) throw new Error('')
    }catch{
      return res.status(404).json({
        msg: "item not found",
      });
    }
    
    const itemId = itemShop.itemId;

    // Check Saldo User
    if (user.money < itemShop.price.buy * qty) {
      return res.status(400).json({
        msg: "You don't have enough a money",
      });
    }

    // Update Item User
    // console.log(itemId);
    const idx = user.items.findIndex((item) => item.itemId.equals(itemId));
    if (idx >= 0) {
      // Jika sudah ada
      user.items[idx].qty += qty;
    } else {
      // Jika belum ada
      user.items.push({ itemId, qty: qty });
    }
    
    // Update Saldo User
    const totalPrice =  itemShop.price.buy * qty; // Harga Akhir
    user.money -= totalPrice;
    await user.save();

    // get Item
    const item = await Item.findById(itemShop.itemId, {
      _id: 0,
      __v: 0
    })

    return res.status(201).send({
      msg: "Thank you for visiting us",
      item,
      totalPrice, 
      money: user.money,
    });
  }
);

// POST /sell
router.post(
  "/sell",
  authUser,
  validateStatus(UserStatus.IDLE),
  async (req, res) => {
    let {
      // destructuring
      shopItemId,
      qty,
      _user: user,
    } = req.body;
    // Check jika Inputan tidak valid
    if (shopItemId == null) {
      return res.status(400).json({
        msg: "missing item id",
      });
    }

    if (!qty || !qty.match("^[0-9]*$")) {
      return res.status(400).json({
        msg: "item quantity is not valid",
      });
    }
    qty = parseInt(qty);

    // Check apakah item dijual pada shop
    let itemShop = null
    try{
      itemShop = await ShopItem.findById(shopItemId);
      if(!itemShop) throw new Error('')
    }catch{
      return res.status(404).json({
        msg: "item not found",
      });
    }

    //dapet item id
    const itemId = itemShop.itemId;

    //nge get di user nya
    const indexitem = user.items.findIndex((item) =>
      item.itemId.equals(itemId)
    );
    //berarti gak ada item di usernya
    if (indexitem < 0) {
      return res.status(404).json({
        msg: "You don't have this item",
      });
    }

    if (user.items[indexitem].qty < qty) {
      return res.status(404).json({
        msg: "You don't have enough item",
      });
    }
    
    // Update qty
    user.items[indexitem].qty -= qty;
    if(user.items[indexitem].qty <= 0){// Remove from User
      user.items.splice(indexitem, 1)
    }

    // Update Saldo User
    const earnMoney = itemShop.price.sell * qty
    user.money += earnMoney;
    await user.save();

    // Get Item yg dijual
    const item = await Item.findById(itemId, { _id: 0, __v: 0 })

    return res.status(201).send({
      msg: "Succesfully sold item!",
      item,
      earnMoney, 
      money: user.money,
    });
  }
);

module.exports = router;
