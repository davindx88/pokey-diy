// Library
const mongoose = require('mongoose')
const express = require('express')
require('dotenv').config()

// Morgan
const morgan = require('morgan');
const { getMessage } = require('./modules/morgan')
morgan.token('dateku',(req,res)=>{
    const today = new Date()
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth()+1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    return dd + '/' + mm + '/' + yyyy;
});
morgan.token('msg',(req,res)=>{return getMessage()});
let format = morgan([
    'Method::method',
    'URL::url',
    'Status::status',
    'Message::msg',
    'DateTime::dateku',
    'ResponseTime::response-time ms',
].join('; '),{});

// Buat Koneksi ke Mongo
mongoose.connect(process.env.MONGODB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

// Make Instance of Server
const app = express()
app.use(format);
app.use(express.urlencoded({ extended: true }));
// Routes
const userRouter = require('./routes/user')
const exploreRouter = require('./routes/explore')
const itemRouter = require('./routes/item')
const pokemonRouter = require('./routes/pokemon')
const shopRouter = require('./routes/shop')
const multiplayerRouter = require('./routes/multiplayer')
const battleRouter = require('./routes/battle')
const egg = require('./routes/egg')
const pokedex = require('./routes/pokedex')

// EndPoint
app.use('/api/user', userRouter)
app.use('/api/explore', exploreRouter)
app.use('/api/item', itemRouter)
app.use('/api/pokemon', pokemonRouter)
app.use('/api/shop', shopRouter)
app.use('/api/multiplayer', multiplayerRouter)
app.use('/api/battle', battleRouter)
app.use('/api/egg', egg)
app.use('/api/pokedex', pokedex)
app.get('/proyek-ai', (req, res)=>{
    res.status(200).send(`
    <!DOCTYPE html>
    <html>
    <head>
        <style>
            iframe{
                height: 100vh;
                width: 100vw;
            }
        </style>
    </head>
    <body>
    <iframe src="https://www.youtube.com/embed/Z1rBA0Mr5KQ">
    </iframe>
    </body>
    </html>
    `)
})

// Start Server
const PORT = process.env.PORT || 3000;
app.listen(PORT)
console.log("running on port : 3000")
