const obj = {
    msg: '',
    getMessage: function(){
        return obj.msg
    },
    setMessage: function(newMsg){
        obj.msg = newMsg
    }
}

module.exports = obj