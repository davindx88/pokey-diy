// Module
const mongoose = require('mongoose')
const { Gender, Nature } = require('../variables')
const { PokedexPokemon } = require('../models')

function levelUpPokemon(pokemon, nLevel) {
	if (nLevel <= 0) return
	levelUpPokeAttribute(pokemon)
	levelUpPokemon(pokemon, nLevel - 1)
}

function levelUpPokeAttribute(pokemon){
	pokemon.level++
	pokemon.atk = Math.round(pokemon.atk * 1.1)
	pokemon.def = Math.round(pokemon.def * 1.1)
	pokemon.spAtk = Math.round(pokemon.spAtk * 1.1)
	pokemon.spDef = Math.round(pokemon.spDef * 1.1)
	pokemon.spd = Math.round(pokemon.spd * 1.1)
	pokemon.maxHp = Math.round(pokemon.maxHp * 1.1)
	pokemon.maxSp = Math.round(pokemon.maxSp * 1.1)
	pokemon.maxExp = pokemon.level * 200
	// heal Pokemon
	pokemon.hp = pokemon.maxHp
	pokemon.sp = pokemon.maxSp
}

function giveExpPokemon(pokemon, exp){
	pokemon.exp += exp
	while(pokemon.exp >= pokemon.maxExp){
		pokemon.exp -= pokemon.maxExp
		levelUpPokeAttribute(pokemon)
	}
}

async function generateEntryPokemon(shinyRate) {
	let entryNo = 0;
	entryNo = Math.floor(Math.random() * 897) + 1; 
	return await generatePokemon(entryNo, shinyRate);
};

async function generatePokemon(entryNo, shinyRate = 0) {
	const pokemon = await PokedexPokemon.findOne({ entryNo })
	// Id
	const _id = mongoose.Types.ObjectId();
	// Status Pokemon
	const { // Attribute P9okemon
		name,
		atk,
		def,
		spAtk,
		spDef,
		spd,
		maxHp,
		maxSp,
		types,
		image
	} = pokemon
	const level = 1
	// Shiny
	const isShiny = Math.random() < shinyRate
	// Gender
	const isFemalable = image.female != null
	const gender = !isFemalable ? Gender.GENDERLESS : Math.random() > 0.5 ? Gender.MALE : Gender.FEMALE
	// Nature
	const natures = []
	for (let nature in Nature) {
		natures.push(Nature[nature])
	}
	const idx = Math.floor(Math.random() * natures.length)
	const nature = natures[idx]

	const newPokemon = {
		_id,
		name,
		entryNo,
		isShiny,
		gender,
		nature,
		types,
		level,
		atk,
		def,
		spAtk,
		spDef,
		spd,
		hp: maxHp,
		maxHp,
		sp: maxSp,
		maxSp,
		exp: 0,
		maxExp: 200
	}

	return newPokemon
}


const PokeFactory = {
	levelUpPokemon,
	generatePokemon,
	generateEntryPokemon,
	giveExpPokemon
}

module.exports = PokeFactory

// Utk nyoba
async function main() {
	const pokemon = await generateEntryPokemon(1)
	giveExpPokemon(pokemon, 300)
	console.log(pokemon)

	// let pokemon = await generatePokemon(1)
	// console.log(pokemon)

}
main()
