const { Item } = require("../models")
const { ItemType } = require("../variables")

function validateActionSwitch(res, pokemonId, trainer){
    if(pokemonId == null){
        res.status(400).json({
            msg: `Missing pokemonId field!`
        })
        return false
    }
    if(pokemonId == trainer.activePokemon._id){ // Apakah pokemon bukan aktif pokemon
        res.status(400).json({
            msg: `Can't switch to active pokemon!`
        })
        return false
    }
    const targetPokemon = trainer.inactivePokemons.find(poke => poke._id == pokemonId)
    if(targetPokemon == null){ // Jika tidak ketemu
        res.status(404).json({
            msg: `Pokemon not found!`
        })
        return false
    }
    if(targetPokemon.hp <= 0){ // jika ketemu tapi darahnya habis
        res.status(400).json({
            msg: `Can't switch to deceased pokemon!`
        })
        return false
    }
    return true
}

async function validateItemBattle(itemId, isWildPokemon){
    const item = await Item.findById(itemId)
    console.log(item.type)
    if(isWildPokemon){ // Jika Wild pokemon
        return [
            ItemType.POTION,
            ItemType.ETHER,
            ItemType.POKEBALL,
        ].includes(item.type)
    }else{ // Jika Multiplayer
        return [
            ItemType.POTION,
            ItemType.ETHER,
        ].includes(item.type)
    }
}

function validateUserHasItem(user, itemId){
    const items = user.items
    const isValid = items.some(item => item.itemId == itemId && item.qty > 0)
    return isValid
}

async function useRestorationItem(user, itemId, pokemon, callback){
    const itemTemplate = await Item.findById(itemId)
    let amountHp = 0
    let amountSp = 0
    if(itemTemplate.type == ItemType.POTION){ // POTION
        if(itemTemplate.isPercentage){
            amountHp = pokemon.maxHp * itemTemplate.amount
        }else{
            amountHp = itemTemplate.amount
        }
    }else{ // ETHER
        if(itemTemplate.isPercentage){
            amountSp = pokemon.maxSp * itemTemplate.amount
        }else{
            amountSp = itemTemplate.amount
        }
    }
    pokemon.hp += amountHp
    pokemon.sp += amountSp
    pokemon.hp = Math.min(pokemon.maxHp, Math.floor(pokemon.hp))
    pokemon.sp = Math.min(pokemon.maxSp, Math.floor(pokemon.sp))

    await updateQtyItem(user, itemId)
    
    callback(amountHp, amountSp)
}

async function usePokeball(user, itemId, wildPokemon, callback){
    const item = await Item.findById(itemId)
    await updateQtyItem(user, itemId)
    let catchRate = item.catchRate
    if(item.element != null){ // Jika element ball
        console.log(wildPokemon.types)
    }   
    const isCaught = Math.random() < (catchRate/100)
    if(isCaught){
        const newPokemon = wildPokemon
        newPokemon.isActive = true
        newPokemon.pokeball = itemId
        user.pokemons.push(newPokemon)
    }
    callback(isCaught)
}   

async function updateQtyItem(user, itemId){
    // Update User Item
    const item = user.items.find(item => item.itemId == itemId)
    item.qty--;
    user.items = user.items.filter(item => item.qty>0)
}

function switchPokemon(trainer, pokemonId){
    const idxTargetPokemon = trainer.inactivePokemons.findIndex(poke => poke._id == pokemonId)
    const targPokemon = trainer.inactivePokemons[idxTargetPokemon]
    const currPokemon = trainer.activePokemon
    trainer.inactivePokemons.splice(idxTargetPokemon, 1)
    trainer.activePokemon = targPokemon
    trainer.inactivePokemons.push(currPokemon)
    return targPokemon
}

function validateBattlePokemon(res, user, pokemons){
	// Check Inputan Kembar
	pokemons = [...new Set(pokemons)] // Unique Pokemon
	if (pokemons.length < 1 || pokemons.length > 6) {
		res.status(400).json({
			msg: 'Must carry at least 1-6 Pokemon(s)!'
		})
        return false;
	}
	// Check Pokemon tidak ditemukan
	const userPokemons = user.pokemons.filter(poke => poke.isActive)
	const userIdPokemons = userPokemons.map(poke => poke._id)
	const isPokemonValid = pokemons.every(idPoke => userIdPokemons.includes(idPoke))
	if (!isPokemonValid) {
		res.status(404).json({
			msg: `There's at least 1 not found pokemon!`
		})
        return false;
	}
	// Checkn Pokemon Mati
	const isPokemonAllAlive = userPokemons.every(poke => poke.hp > 0)
	if (!isPokemonAllAlive) {
		res.status(400).json({
			msg: `Cant carry pokemon with 0 health!`
		})
        return false;
	}
    return true;
}

module.exports = {
    validateItemBattle,
    validateUserHasItem,
    useRestorationItem,
    usePokeball,
    switchPokemon,
    validateActionSwitch,
    validateBattlePokemon
}