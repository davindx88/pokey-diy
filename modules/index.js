// Import dulu modulenya apa sekalian rename kalau perlu
const exampleModule = require('./example')
const PokeFactory = require('./pokeFactory')

// Export
module.exports = {
    exampleModule,
    PokeFactory
}