const ExpeditionType = {
    EGG: 'egg',
    ITEM: 'item',
    WILDPOKEMON: 'wild-pokemon',
    TRAINER: 'trainer',
};
module.exports = ExpeditionType;
  