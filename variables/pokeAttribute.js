const PokeAttribute = {
    LEVEL:'level',
	ATK:'atk',
	DEF:'def',
	SPATK:'spAtk',
	SPDEF:'spDef',
	SPD:'spd',
	HP:'hp',
	MAXHP:'maxHp',
	SP:'sp',
	MAXSP:'maxSp',
}

module.exports = PokeAttribute
