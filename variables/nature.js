const Nature = {
    HARDY: 'hardy',
    BOLD: 'bold',
    MODEST: 'modest',
    CALM: 'calm',
    TIMID: 'timid',
    LONELY: 'lonely',
    DOCILE: 'docile',
    MILD: 'mild',
    GENTLE: 'gentle',
    HASTY: 'hasty',
    ADAMANT: 'adamant',
    IMPISH: 'impish',
    BASHFUL: 'bashful',
    CAREFUL: 'careful',
    RASH: 'rash',
    JOLLY: 'jolly',
    NAUGHTY: 'naughty',
    LAX: 'lax',
    QUIRKY: 'quirky',
    NAIVE: 'naive',
}

module.exports = Nature
