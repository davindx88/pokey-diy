const UserStatus = {
    IDLE: 'idle',
    EXPEDITION: 'expedition',
    ROOM: 'room',
    MATCHING: 'matching',
    MULTIPLAYER: 'multiplayer'
}

module.exports = UserStatus
