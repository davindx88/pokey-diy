const BattleResult = {
    WINBATTLE: 'win battle',
    LOSEBATTLE: 'lose battle',
    RUN: 'run',
    CATCHPOKEMON: 'catch pokemon',
    ONGOING: 'ongoing'
}
module.exports = BattleResult;