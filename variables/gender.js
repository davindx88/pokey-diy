const Gender = {
    MALE: 'male',
    FEMALE: 'female',
    GENDERLESS: 'genderless',
}

module.exports = Gender
