const PokedexStatus = {
    CAUGHT:'caught',
    SEEN:'seen',
    NOTFOUND: 'not found',
}

module.exports = PokedexStatus
