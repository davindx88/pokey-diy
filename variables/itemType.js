const ItemType = {
    POTION: 'potion', 
    ETHER: 'ether', 
    REVIVE: 'revive', 
    POKEBALL: 'pokeball', 
    NUTRIENT: 'nutrient',
    STONE: 'stone',
}

module.exports = ItemType
