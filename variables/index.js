// Import
const UserStatus = require('./userStatus')
const ItemType = require('./itemType')
const PokeAttribute = require('./pokeAttribute')
const ElementType = require('./elementType')
const Gender = require('./gender')
const Nature = require('./nature')
const PokedexStatus = require('./pokedexStatus')
const ExpeditionType = require('./expeditionType')
const BattleAction = require('./battleAction')
const BattleResult = require('./battleResult')
const MultiplayerStatus = require('./multiplayerStatus')
// Export
module.exports = {
    UserStatus,
    ItemType,
    PokeAttribute,
    ElementType,
    Gender,
    Nature,
    PokedexStatus,
    ExpeditionType,
    BattleAction,
    BattleResult,
    MultiplayerStatus
}
