const MultiplayerStatus = {
    WAITING: 'waiting',
    ONGOING: 'ongoing',
    FINISH: 'finish'
}
module.exports = MultiplayerStatus;