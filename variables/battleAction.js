const BattleAction = {
    ATTACK: 1, // Atk biasa
    SPECIAL: 2, // Special Attack
    RUN: 3, // KAbur / surender
    SWITCH: 4, // Switch Pokemon (pokemon)
    ITEM: 5, // Pake Item (heal, ether, pokeball)
    ENDBATTLE: 99, // Digunakan untuk mengakhiri battle
};

BattleAction.toString = (actionIdx)=>{
    if(actionIdx == 1) return 'attack'
    if(actionIdx == 2) return 'special attack'
    if(actionIdx == 3) return 'run'
    if(actionIdx == 4) return 'switch pokemon'
    if(actionIdx == 5) return 'use item'
    if(actionIdx == 99) return 'end battle'
    return 'unknown'
}

module.exports = BattleAction;
  