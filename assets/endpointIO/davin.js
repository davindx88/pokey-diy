let obj = {}

//// POST {{URL}}/explore/
// INPUT
// x-auth-token
obj = {
    difficulty: 'easy', // easy, medium, hard
    pokemons: [ // ID Pokemonnya
        'id1',
        'id2'
    ]
}
// OUTPUT
obj = { // Jika Sudah Exploration Limit
    status: 'explore-limit',
    message: 'Already reached exploration limit!'
}
obj = { // Jika Pokemon tidak ditemukan
    status: 'pokemon-not-found',
    message: 'There\'s something wrong with Input'
}
obj = { // Jika Pokemon mati
    status: 'pokemon-is-dead',
    message: 'There\'s something wrong with Input'
}
obj = { // Jika Mendapat Item
    status: 'found-item',
    items: [
        { name: 'item1', qty: 1, description: 'desc item 1', image: 'https://picsum.photos/200' },
        { name: 'item2', qty: 2, description: 'desc item 2', image: 'https://picsum.photos/200' },
        { name: 'item3', qty: 3, description: 'desc item 3', image: 'https://picsum.photos/200' },
    ]
}
obj = { // Jika Ketemu Egg
    status: 'found-egg',
    egg: {
        name: 'very rare egg',
        description: 'this is an very rare egg',
        image: 'https://picsum.photos/200'
    }
}
obj = { // Jika ketemu Pokemon Liar
    status: 'found-wild-pokemon',
    wild_pokemon: {
        name: 'Bulbasaur',
        image: 'https://picsum.photos/200',
    },
}
obj = { // Jika ketemu Trainer
    status: 'found-trainer',
    trainer: {
        name: 'Trainer X',
        image: 'https://picsum.photos/200'
    }
}

//// GET {{URL}}/explore/status
// Input
// x-auth-token
// Output
obj = { // Jika tidak lagi explore
    status: 'idle',
    remaining_explore: 5
}
obj = { // Jika lagi battle dengan Wild Pokemon
    status: 'pokemon-battle',
    remaining_explore: 5,
    difficulty: 'easy', // easy medium hard
    wild_pokemon: {
        name: 'Bulbasaur',
        level: 5,
        hp: 100,
        max_hp: 100,
        gender: 'female', // male, female, unknown
        is_owned: false,
        image: 'https://picsum.photos/200',
    },
    player: {
        name: 'Ash Ketchum',
        image: 'https://picsum.photos/200',
        active_pokemon: {
            name: 'CharChar',
            level: 10,
            hp: 100,
            max_hp: 200,
            sp: 1,
            max_sp: 10,
            gender: 'male', // male, female, unknown
            is_alive: true,
            image: 'https://picsum.photos/200'
        },
        inactive_pokemons: [
            {
                name: 'CharChar',
                level: 10,
                hp: 100,
                max_hp: 200,
                sp: 1,
                max_sp: 10,
                gender: 'male', // male, female, unknown
                is_alive: false,
                image: 'https://picsum.photos/200'
            }
        ],
        items: [
            { name: 'item 1', qty: 1, description: 'item 1 ini gunanya apa', image: 'https://picsum.photos/200' },
            { name: 'item 2', qty: 2, description: 'item 2 ini gunanya apa', image: 'https://picsum.photos/200' },
            { name: 'item 3', qty: 3, description: 'item 3 ini gunanya apa', image: 'https://picsum.photos/200' },
        ]
    }
}
obj = { // Jika lagi battle dengan Trainer
    status: 'trainer-battle',
    remaining_explore: 5,
    difficulty: 'easy', // easy medium hard
    trainer: {
        name: 'Trainer 1',
        image: 'https://picsum.photos/200',
        active_pokemon: {
            name: 'CharChar',
            level: 10,
            hp: 100,
            max_hp: 200,
            gender: 'male', // male, female, unknown
            is_owned: false,
            image: 'https://picsum.photos/200'
        },
        number_of_pokemon: 3,
        number_of_alive_pokemon: 1
    },
    player: {
        name: 'Ash Ketchum',
        image: 'https://picsum.photos/200',
        active_pokemon: {
            name: 'CharChar',
            level: 10,
            hp: 100,
            max_hp: 200,
            sp: 1,
            max_sp: 10,
            gender: 'male', // male, female, unknown
            is_alive: true,
            image: 'https://picsum.photos/200'
        },
        inactive_pokemons: [
            {
                name: 'CharChar',
                level: 10,
                hp: 100,
                max_hp: 200,
                sp: 1,
                max_sp: 10,
                gender: 'male', // male, female, unknown
                is_alive: false,
                image: 'https://picsum.photos/200'
            }
        ],
        items: [
            { name: 'item 1', qty: 1, description: 'item 1 ini gunanya apa', image: 'https://picsum.photos/200' },
            { name: 'item 2', qty: 2, description: 'item 2 ini gunanya apa', image: 'https://picsum.photos/200' },
            { name: 'item 3', qty: 3, description: 'item 3 ini gunanya apa', image: 'https://picsum.photos/200' },
        ]
    }
}

//// POST {{URL}}/explore/action
// INPUT
obj = {
    action: 'attack'
}
obj = {
    action: 'skill'
}
obj = {
    action: 'use-item',
    item: 'pokeball', // nama item
}
obj = {
    action: 'run'
}
obj = {
    action: 'swap-pokemon', // choose-pokemon
    pokemon: 'id_pokemon'
}
// OUTPUT
obj = { // Jika tidak sedang explore
    status: 'idle',
    message: 'Is not exploring'
}
obj = { // Jika

}

//// GET {{URL}}/explore/history
//// GET {{URL}}/explore/history/:id

//// GET {{URL}}/multiplayer/room
// INPUT
// .. nothing
// OUTPUT
obj = {
    rooms: [
        {
            id: 'XL12S', // ID Auto Generate bkn ID Mongo
            name: 'room 1',
            host: {
                name: 'Davin Djayadi',
                image: ''
            }
        }
    ]
}

//// POST {{URL}}/multiplayer/room
// INPUT
// x-auth-token
obj = {
    name: 'Custom Room',
    password: 'hehe',
    is_public: true
}
// OUTPUT
obj = { // Jika unavailable
    status: 'user-busy',
    message: 'user currently busy'
}
obj = { // Jika success dibuat
    status: 'room-created',
    message: 'room successfully created!',
    room: {
        id: 'XL12S',
        name: 'Custom Room'
    }
}
// NOTE
/*
    Unavailable bisa jadi karena user:
    - Sedang explore
    - Sedang Ada pada room
    - Sedang Battle
*/

//// POST {{URL}}/multiplayer/room/join
// INPUT
obj = {
    room_id: 'XL12S', // Room Id
    password: '', // Jika ada
}
// OUTPUT
obj = { // Jika not available
    status: 'user-busy',
    message: 'user currently busy'
}
obj = { // Jika room tidak ditemukan
    status: 'room-not-found',
    message: 'room not found!'
}
obj = { // Jika room penuh
    status: 'room-full',
    message: 'room is currently full!'
}
obj = { // Jika berhasil join
    status: 'room-joined',
    message: 'successfully joined',
    room: {
        id: 'XL12S',
        name: 'Roomku',
        host: {
            name: 'Ash',
            image: 'https://picsum.photos/200',
            number_of_pokemon: 3
        }
    }
}

//// POST {{URL}}/multiplayer/room/set-team
// INPUT
obj = {
    pokemons: [ // ID Pokemon yang dipilih
        'id1',
        'id2'
    ]
}
// OUTPUT
obj = { // Jika tidak pada room
    status: 'not-in-room',
    message: 'Player not in room'
}
obj = { // Jika Pokemon tidak ditemukan
    status: 'pokemon-not-found',
    message: 'There\'s something wrong with Input'
}
obj = { // Jika Pokemon mati
    status: 'pokemon-is-dead',
    message: 'There\'s something wrong with Input'
}
obj = { // Jika Berhasil Set Team
    status: 'team-set',
    message: 'Team successfully setted!'
}

//// POST {{URL}}/multiplayer/room/start
// INPUT
// x-auth-token
// OUTPUT
obj = { // Jika tidak pada room
    status: 'not-in-room',
    message: 'Player currently not in room'
}
obj = { // Player bukan Host
    status: 'not-host',
    message: 'Player is not a host'
}
obj = { // Ada Player yang blm set-up team
    status: 'not-ready',
    message: 'Not all player ready'
}
obj = { // Jika Kedua player sudah ready
    status: 'battle-begin',
    message: 'Battle begin'
} 
// NOTE
// nanti lgsg di redirect ke battle dan attribute battle bisa diapat dari
// -> GET {{URL}}/battle

//// POST {{URL}}/multiplayer/match
// INPUT
// x-auth-token
obj = {
    pokemons: [ // id pokemon
        'id1',
        'id2'
    ]
}
// OUTPUT
obj = { // Jika Pokemon tidak ditemukan
    status: 'pokemon-not-found',
    message: 'There\'s something wrong with Input'
}
obj = { // Jika Pokemon mati
    status: 'pokemon-is-dead',
    message: 'There\'s something wrong with Input'
}
obj = { // Jika Player sibuk
    status: 'user-busy',
    message: 'User is not available!'
}
obj = { // Jika berhasil matching
    status: 'begin-matching',
    message: 'User matching'
}

//// GET {{URL}}/multiplayer/status
// INPUT
// x-auth-token
// OUTPUT
obj = { // Jika Idle
    status: 'idle',
    message: 'user idle'
}
obj = { // Jika user pada room
    status: 'user-in-room',
    room: {
        id: 'XL12S',
        name: 'Roomku',
        is_host: false,
        host: {
            name: 'ash k',
            image: 'https://picsum.photos/200'
        },
        player: {
            name: 'ash',
            image: 'https://picsum.photos/200',
            number_of_pokemon: 3,
            pokemons: [
                { 
                    name: 'Poke 1', 
                    level: 100, 
                    hp: 12, 
                    max_hp: 20, 
                    gender: 'male', // male female unknown
                }
            ]
        },
        other_player: {
            name: 'ash k',
            image: 'https://picsum.photos/200',
            number_of_pokemon: 3
        }
    }
}
obj = { // Jika user masih matching
    status: 'matching',
    message: 'Searching opponent'
}

//// POST {{URL}}/multiplayer/leave
// INPUT
// x-auth-token
// OUTPUT
obj = { // Jika User tidak pada Room / Mathing
    status: 'idle',
    message: 'User currently idle'
}
obj = { // Jika User sedang sibuk
    status: 'user-busy',
    message: 'User is currently not available'
}
obj = { // Jika User sedang Battle
    status: 'user-battle',
    message: 'User is currently Battle'
}
obj = { // Jika user berhasil leave room
    status: 'leave-room',
    message: 'User leave room XL12S'
}
obj = { // JIka user berhasil cancel matching
    status: 'cancel-match',
    message: 'Matching cancelled'
}
// NOTE
// hanya bisa leave room / matching saja

//// GET {{URL}}/battle
// INPUT
// x-auth-token
// OUTPUT
obj = { // Jika user tidak battle
    status: 'idle',
    message: 'User is Idle'
}
obj = { // Dapetin Battle Status saat ini
    status: 'is-battle',
    is_ranked: true,
    is_player_turn: true,
    player: {
        name: 'Ash Ketchum',
        image: 'https://picsum.photos/200',
        active_pokemon: {
            name: 'CharChar',
            level: 10,
            hp: 100,
            max_hp: 200,
            sp: 1,
            max_sp: 10,
            gender: 'male', // male, female, unknown
            is_alive: true,
            image: 'https://picsum.photos/200'
        },
        inactive_pokemons: [
            {
                name: 'CharChar',
                level: 10,
                hp: 100,
                max_hp: 200,
                sp: 1,
                max_sp: 10,
                gender: 'male', // male, female, unknown
                is_alive: false,
                image: 'https://picsum.photos/200'
            }
        ],
    },
    opponent: {
        name: 'Gary',
        image: 'https://picsum.photos/200',
        active_pokemon: { // bisa jadi null
            name: 'CharChar',
            level: 10,
            hp: 100,
            max_hp: 200,
            gender: 'male', // male, female, unknown
            is_owned: true,
            image: 'https://picsum.photos/200'
        },
        number_of_pokemon: 3,
        number_of_alive_pokemon: 1
    }
}
obj = { // Jika Battle baru saja berakhir
    status: 'battle-end',
    is_win: false,
    winner: {
        name: 'Ash Ketchum',
        image: 'https://picsum.photos/200'
    },
    loser: {
        name: 'Gary',
        image: 'https://picsum.photos/200'
    },
    old_rank: 10,
    new_rank: 20
}

//// POST {{URL}}/battle
// INPUT
// x-auth-token
obj = {
    action: 'attack', // attack, skill, swap-pokemon, surrender
}
obj = {
    action: 'swap-pokemon', // choose-pokemon
    pokemon: '', // ID pokemon
}
// OUTPUT
obj = { // Jika player sedang sibuk
    status: 'user-busy',
    message: 'user currently busy'
}
obj = { // Jika bukan giliran player
    status: 'not-player-turn',
    message: 'Not player turn'
}
obj = { // Jika player surender
    status: 'surender',
    message: 'Player surender',
    opponent: {
        name: 'Gary',
        image: 'https://picsum.photos/200',
    },
    is_win: false,
    old_rank: 20,
    new_rank: 10
}
obj = { // JIka attack / skill
    status: 'player-attack', // player-attack / player-skill
    message: 'Pikachu attack Charmander', // use skill attack
    damage: 10,
    opponent: {
        name: 'Gary',
        image: 'https://picsum.photos/200',
        active_pokemon: { 
            name: 'CharChar',
            level: 10,
            hp: 0,
            max_hp: 200,
            gender: 'male', // male, female, unknown
            is_owned: true,
            image: 'https://picsum.photos/200'
        },
        number_of_pokemon: 3,
        number_of_alive_pokemon: 1
    },
    // Jika Menang atau kalah
    is_win: true,
    old_rank: 10,
    new_rank: 20
}
obj = { // JIka Swap Pokemon
    status: 'swap-pokemon',
    message: 'Pokemon swapped!',
    player: {
        name: 'Ash Ketchum',
        image: 'https://picsum.photos/200',
        active_pokemon: {
            name: 'CharChar',
            level: 10,
            hp: 100,
            max_hp: 200,
            sp: 1,
            max_sp: 10,
            gender: 'male', // male, female, unknown
            is_alive: true,
            image: 'https://picsum.photos/200'
        },
        inactive_pokemons: [
            {
                name: 'CharChar',
                level: 10,
                hp: 100,
                max_hp: 200,
                sp: 1,
                max_sp: 10,
                gender: 'male', // male, female, unknown
                is_alive: false,
                image: 'https://picsum.photos/200'
            }
        ],
    },
}

//// GET {{URL}}/battle/history
//// GET {{URL}}/battle/history/:id

//// Kurang
/*
History
Attack Pokemon Feedback
Action Explore
user.name -> user.username
*/