// =======================================================================================================================================================
// Item  => {{URL}}/item
//output
obj = {
  itemUser: [
    {
      _id: "60b3cd296f8cad2c7c148d4f",
      item: {
        name: "potion",
        description: "Restores 20 HP.",
        image:
          "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/potion.png",
        type: "potion",
        isPercentage: false,
        amountHp: 20,
      },
      qty: 2,
    },
    {
      _id: "60b482ec8fa77642f4acc672",
      item: {
        name: "max-potion",
        description: "Restores HP to full",
        image:
          "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/max-potion.png",
        type: "potion",
        isPercentage: true,
        amountHp: 100,
      },
      qty: 1, //jumlah item yang dimiliki oleh seorang user.
    },
  ],
};

//output jika user tidak ada
let message = {
  message: "User not found", // di handel di authuser
};

//jika tidak memiliki item
let message = {
  message: "You don't have any item",
};
// =======================================================================================================================================================

//Open Shop => GET	{{URL}}/shop
//Output
obj = {
  items: [
    {
      _id: "60ba0c8729b6091ddc452436",
      itemId: "60ba0c8729b6091ddc45242c",
      price: {
        buy: 200,
        sell: 100,
      },
      item: {
        name: "potion",
        description: "Restores 20 HP.",
        image:
          "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/potion.png",
        type: "potion",
        isPercentage: false,
        amount: 20,
      },
      QtyItemUser: 40, // ini adaalah jumlah item yang dimiliki oleh user
    },
    {
      _id: "60ba0c8729b6091ddc452437",
      itemId: "60ba0c8729b6091ddc45242d",
      price: {
        buy: 2500,
        sell: 1250,
      },
      item: {
        name: "max-potion",
        description: "Restores HP to full",
        image:
          "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/max-potion.png",
        type: "potion",
        isPercentage: true,
        amount: 100,
      },
      QtyItemUser: 20,
    },
    {
      _id: "60ba0c8729b6091ddc452438",
      itemId: "60ba0c8729b6091ddc45242e",
      price: {
        buy: 300,
        sell: 600,
      },
      item: {
        name: "ether",
        description: "Restores 10 SP",
        image:
          "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/ether.png",
        type: "ether",
        isPercentage: false,
        amount: 10,
      },
      QtyItemUser: 0,
    },
    {
      _id: "60ba0c8729b6091ddc452439",
      itemId: "60ba0c8729b6091ddc45242f",
      price: {
        buy: 3000,
        sell: 1150,
      },
      item: {
        name: "max-ether",
        description: "Restores SP to full",
        image:
          "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/max-ether.png",
        type: "ether",
        isPercentage: true,
        amount: 100,
      },
      QtyItemUser: 0,
    },
    {
      _id: "60ba0c8729b6091ddc45243a",
      itemId: "60ba0c8729b6091ddc452430",
      price: {
        buy: 2000,
        sell: 1000,
      },
      item: {
        name: "revive",
        description: "Revives with half HP.",
        image:
          "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/revive.png",
        type: "revive",
        isPercentage: true,
        amount: 50,
      },
      QtyItemUser: 0,
    },
    {
      _id: "60ba0c8729b6091ddc45243b",
      itemId: "60ba0c8729b6091ddc452431",
      price: {
        buy: 4000,
        sell: 2000,
      },
      item: {
        name: "max-revive",
        description: "Revives with full HP.",
        image:
          "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/max-revive.png",
        type: "revive",
        isPercentage: true,
        amount: 100,
      },
      QtyItemUser: 0,
    },
    {
      _id: "60ba0c8729b6091ddc45243c",
      itemId: "60ba0c8729b6091ddc452432",
      price: {
        buy: 200,
        sell: 100,
      },
      item: {
        name: "poke-ball",
        description: "Tries to catch a wild Pokémon.",
        image:
          "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/poke-ball.png",
        type: "pokeball",
        catchRate: 25,
      },
      QtyItemUser: 0,
    },
    {
      _id: "60ba0c8729b6091ddc45243d",
      itemId: "60ba0c8729b6091ddc452433",
      price: {
        buy: 1000,
        sell: 500,
      },
      item: {
        name: "poke-ball",
        description: "Tries to catch a wild Pokémon.",
        image:
          "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/poke-ball.png",
        type: "pokeball",
        catchRate: 25,
        element: {
          types: ["fire", "flying"],
          catchRate: 55,
        },
      },
      QtyItemUser: 0,
    },
    {
      _id: "60ba0c8729b6091ddc45243e",
      itemId: "60ba0c8729b6091ddc452434",
      price: {
        buy: 10000,
        sell: 5000,
      },
      item: {
        name: "hp-up",
        description: "Increases HP effort by 10",
        image:
          "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/hp-up.png",
        type: "nutrient",
        attribute: "hp",
        amount: 10,
      },
      QtyItemUser: 0,
    },
    {
      _id: "60ba0c8729b6091ddc45243f",
      itemId: "60ba0c8729b6091ddc452435",
      price: {
        buy: 5000,
        sell: 0,
      },
      item: {
        name: "fire-stone",
        description: "Evolve fire pokemon",
        image:
          "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/fire-stone.png",
        type: "stone",
        elementType: "fire",
      },
      QtyItemUser: 0,
    },
  ],
};
//output jika user tidak ada
let message = {
  message: "Id user not found",
};
// =======================================================================================================================================================

//Beli item =>  POST	{{URL}}/shop/buy
//Input
let shopItemId = 0;
let qty = 0; //jumlah barang yang ingin di beli

//Output
obj = {
  message: "Thank you for visiting us",
  player_money: 100,
};

//output jika id tidak ditemukan
let message = {
  message: "missing item id",
};
//output jika id barang tidak ditemukan
let message = {
  message: "item not found",
};

//output jika uang tidak cukup
let message = {
  message: "You don't have enough money",
};

//output jika quantity tidak cukup
let message = {
  message: "item quantity is not valid",
};

//Sell item =>  POST	{{URL}}/shop/sell
//Input
let shopItemId = 0;
let qty = 0; //jumlah barang yang ingin di beli

//Output
obj = {
  message: "Thank you for visiting us",
  player_money: 100,
};

//output jika id tidak ditemukan
let message = {
  message: "missing item id",
};
//output jika id barang tidak ditemukan
let message = {
  message: "item not found",
};

//output jika item yang dimiliki user  tidak cukup
let message = {
  message: "You don't have enough item",
};
//output jika quantity tidak cukup
let message = {
  message: "item quantity is not valid",
};

// =======================================================================================================================================================

//Melihat semua telur => Egg {{URL}}/egg
//Input

//Output
let obj = {
  eggUser: [
    {
      _id: "60b503de56845c4be4715370",
      eggId: "60b502999917a40f00881501",
      image: "/assets/images/egg/egg1",
      exploreRemaining: 100,
    },
    {
      _id: "60b503ea56845c4be4715371",
      eggId: "60b502999917a40f00881501",
      image: "/assets/images/egg/egg1",
      exploreRemaining: 100,
    },
  ],
};

//output jika id tidak ditemukan
let message = {
  message: "User not found", // udah di handel sma autuser
};

//output jika tidak memiliki telur
let message = {
  message: "You don't have  egg",
};
// =======================================================================================================================================================

//Melihat detail telur =>  Egg {{URL}}/egg/:id
//input
let iduser;
let idegg;
//output
obj = {
  detailEgg: {
    _id: "60b503de56845c4be4715370",
    egg: {
      name: "Normal-Egg",
      description: "This is a normal egg",
      price: 1000,
    },
    image: "/assets/images/egg/egg1",
    exploreRemaining: 100,
  },
};

//output error
//misal tidak ada id user
let message = {
  message: "User not found",
};

//misal tidak ada id telur
let message = {
  message: "Egg not found",
};
// =======================================================================================================================================================

//Menetaskan telur =>  {{URL}}/egg/:id/hatch
//input
let iduser;
let idegg;
//output
obj = {
  message: "Congratulations your egg hatched",
  pokemon: {
    _id: "60b5365edcf2d11650c61d82",
    name: "swinub",
    entryNo: 220,
    isShiny: true,
    gender: "male",
    nature: "quirky",
    types: ["ice", "ground"],
    level: 1,
    atk: 50,
    def: 40,
    spAtk: 30,
    spDef: 30,
    spd: 50,
    hp: 50,
    maxHp: 50,
    sp: 10,
    maxSp: 10,
  },
};

//output error
//misal tidak ada id user
let message = {
  message: "user not found",
};

//misal tidak ada id telur
let message = {
  message: "Egg not found",
};

//misal belum waktunya namun dipaksa
let message = {
  message: "This egg doesn't ready to hatch",
};
// =======================================================================================================================================================

//Menjual telur => {{URL}}/egg/:id/sell
//input
let idegg;
//output
let message = {
  message: "Bye bye my beloved egg",
  moneyleft: 10,
};

//output error
//misal tidak ada id user
let message = {
  message: "user not found",
};

//misal tidak ada id telur
let message = {
  message: "Egg not found",
};

array.forEach(element => {
  
});
// =======================================================================================================================================================

//Melihat semua pokedex => {{URL}}/pokedex
//output
let object ={allPokemon: [
  {
    entryNo: "354",
    name: "????",
    types: ["????"],
    status: "????",
    image: "https://assets.thespinoff.co.nz/1/2019/04/HddtBOT.png",
  },
  {
    entryNo: 355,
    name: "duskull",
    types: ["ghost"],
    status: "caught",
    image: {
      male: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/355.png",
      female: null,
      shinyMale:
        "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/355.png",
      shinyFemale: null,
    },
  },
  {
    entryNo: "356",
    name: "????",
    types: ["????"],
    status: "????",
    image: "https://assets.thespinoff.co.nz/1/2019/04/HddtBOT.png",
  },
  {
    entryNo: "357",
    name: "????",
    types: ["????"],
    status: "????",
    image: "https://assets.thespinoff.co.nz/1/2019/04/HddtBOT.png",
  },
  {
    entryNo: "358",
    name: "????",
    types: ["????"],
    status: "????",
    image: "https://assets.thespinoff.co.nz/1/2019/04/HddtBOT.png",
  },
]};
//output jika iduser tidak ditemukan
let message = "User not found";

// =======================================================================================================================================================

//Melihat detail pokedex => {{URL}}/pokedex/:id
let idpokedex = id;

let object = {
  detail_pokemon: [
    {
      id_pokemon: 20,
      name: "charmender",
      types: ["fire", "water"],
      itemupgrade: "1x leafstone",
      seen: true,
      evolutions: [
        { name: "charmander", types: [], image: "http" },
        { name: "?", requirement: "3x Fire Stone" },
      ],
      image: "",
    },
  ],
};

//output jika id user tidak ditemukan
let message = "User not found";
//output jika id pokemon tidak ditemukan/dia belum pernah melihat sama sekali
let message = "Pokemon not found";
