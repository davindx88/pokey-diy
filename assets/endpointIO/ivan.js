user={
    username:"Ecube",
    image:"/file/foto.jpg",
    pokemon_count:100,
    status:"Premium",
    money:500000,
    starting_date:"12/05/2021",
    explore_count:10000,
    multiplayer_count:60,
    winrate:70
}

pokemon={
    name:"pikatot",
    id_pokemon:25,
    level:60,
    atk:125,
    def:60,
    sp_def:50,
    sp_atk:70,
    max_hp:1250,
    xp:126,
    max_exp:12000,
    hp:300,
    spd:125,
    max_sp:50,
    sp:20,
    types:["fire","lightning"],
    gender:"male",
    nature:"sassy",
    image:"/folder/25.jpg",
    pokeball:"master ball",
    pokeball_image:"/folder/masterball.png"
}
let obj
let msg
let data

//#region User
    //#region Register
        //INPUT
            obj = {
                username:"Ecube",
                password:"cobadulu",
                pokemon: 0, // Bulba, Squirtle, Charmander 
                image:"/file/foto.jpg",
            }
        //OUTPUT
            msg="Registration Success"
        //ERROR
            msg="Failed Registration"
    //#endregion
    //#region Login
        //INPUT
            obj = {
                username:"Ecube",
                password:"cobadulu"
            }
        //OUTPUT
            obj = {
                token:"dlnFfasd123545dfsa",
            }
        //ERROR
            msg="Username/Password Wrong"
            
    //#endregion
    //#region Get User
        //INPUT
        //OUTPUT
            user={
                username:"Ecube",
                image:"/file/foto.jpg",
                pokemon_count:100,
                status:"Premium",
                money:500000,
                starting_date:"12/05/2021",
                explore_count:10000,
                multiplayer_count:60,
                winrate:70
            }
        //ERROR
            msg="User Not Found"
    //#endregion
    //#region Upgrade User
        //INPUT
        //OUTPUT
            msg="Thanks for upgrading"
        //ERROR
            msg="Failed to Upgrade"
    //#endregion
//#endregion

//#region Pokemon
    //#region Get Pokemon
        //INPUT
        //OUTPUT
            data=[
                {
                    name:"pikatot",
                    id_pokemon:25,
                    level:60,
                    atk:125,
                    def:60,
                    sp_def:50,
                    sp_atk:70,
                    max_hp:1250,
                    hp:300,
                    spd:125,
                    max_sp:50,
                    sp:20,
                    types:["fire","lightning"],
                    gender:"male",
                    nature:"sassy",
                    image:"/folder/25.jpg",
                    pokeball:"master ball",
                    pokeball_image:"/folder/masterball.png"

                },
                {
                    name:"Chatot",
                    id_pokemon:441,
                    level:60,
                    atk:125,
                    def:60,
                    sp_def:50,
                    sp_atk:70,
                    max_hp:1250,
                    hp:300,
                    spd:125,
                    max_sp:50,
                    sp:20,
                    types:["flying","fire"],
                    gender:"male",
                    nature:"sassy",
                    image:"/folder/441.jpg",
                    pokeball:"master ball",
                    pokeball_image:"/folder/masterball.png"

                }
            ]
        //ERROR
            msg="You still dont have pokemon"
    //#endregion
    //#region Get Specified Pokemon
        //INPUT
        //OUTPUT
            data={
                name:"pikatot",
                id_pokemon:25,
                level:60,
                atk:125,
                def:60,
                sp_def:50,
                sp_atk:70,
                max_hp:1250,
                hp:300,
                spd:125,
                max_sp:50,
                sp:20,
                types:["fire","lightning"],
                gender:"male",
                nature:"sassy",
                image:"/folder/25.jpg",
                pokeball:"master ball",
                pokeball_image:"/folder/masterball.png"

            }
        //ERROR
            msg="No Pokemon Found"

    //#endregion
    //#region Edit Specified Pokemon
        //INPUT
            obj={
                name:"pikachudong",
            }
        //OUTPUT
            data={
                name:"pikatot",
                id_pokemon:25,
                level:60,
                atk:125,
                def:60,
                sp_def:50,
                sp_atk:70,
                max_hp:1250,
                hp:300,
                spd:125,
                max_sp:50,
                sp:20,
                types:["fire","lightning"],
                gender:"male",
                nature:"sassy",
                image:"/folder/25.jpg",
                pokeball:"master ball",
                pokeball_image:"/folder/masterball.png"

            }
        //ERROR
            msg="Pokemon not found"
    //#endregion
    //#region Upgrade Specified Pokemon
        //INPUT
            obj={
                hp:1260,
            }
        //OUTPUT
            data={
                name:"pikatot",
                id_pokemon:25,
                level:60,
                atk:125,
                def:60,
                sp_def:50,
                sp_atk:70,
                max_hp:1260,
                hp:300,
                spd:125,
                max_sp:25,
                sp:20,
                types:["fire","lightning"],
                gender:"male",
                nature:"sassy",
                image:"/folder/25.jpg",
                pokeball:"master ball",
                pokeball_image:"/folder/masterball.png"

            }
        //ERROR
            msg="Failed to Upgrade"
    //#endregion
    //#region Sell Specified Pokemon
        //INPUT
        //OUTPUT
            msg="Pokemon sold for $600"
        //ERROR
            msg="Pokemon not found"
    //#endregion
    //#region Evolve Specified Pokemon
        //INPUT
            data={
                pokemon: '' // Object ID Pokemon dari DB
            }
        //OUTPUT
            data={
                name:"pikatot",
                id_pokemon:26,
                level:60,
                atk:150,
                def:70,
                sp_def:85,
                sp_atk:90,
                max_hp:1350,
                hp:300,
                spd:125,
                max_sp:50,
                sp:20,
                types:["fire","lightning"],
                gender:"male",
                nature:"sassy",
                image:"/folder/25.jpg",
                pokeball:"master ball",
                pokeball_image:"/folder/masterball.png"

            }
        //ERROR
            msg="pokemon got no evolution/maxed"
    //#endregion
//#endregion