// User
const User = {
    username: String,
    password: String,
    image: String,
    money: Number,
    is_premium: Boolean,
    explore_count: Number, // Total Explore selamat ini
    remaining_explore: Number, // Explore hari ini
    multiplayer_count: Number, // Total Multiplayer selama ini
    winrate: Number, // Winrate dari Multiplayer
    starting_date: Date, // User buat account kapan
    pokemons: [{
        _id: String, // Object ID
        entry_no: Number,
        is_shiny: Boolean, // apakah shiny atau tidak
        name: String,
        level: Number,
        atk: Number,
        def: Number,
        sp_atk: Number,
        sp_def: Number,
        spd: Number,
        hp: Number,
        max_hp: Number,
        sp: Number,
        max_sp: Number,
        xp: Number,
        max_exp: Number,
        types: [String],
        gender: String, // male female no-gender
        nature: String,
        pokeball: String, // Item ID dari Pokeball
        is_active: Boolean // Jika tidak active maka pokemon sudah dijual
    }],
    items: [{
        item_id: String, // Id Itemnya apa
        qty: Number // Punya sebanyak berapa
    }],
    eggs: [{
        egg_id: String, // ID Dari Egg yang ditemukan
        image_idx: Number, 
        explore_remaining: Number, // Kurang explore berapa kali
    }],
    pokedex: {}, // Key: entry no, Value(String): seen / caught
    expeditions: [{ // Semua expedition yang pernah dilakukan
        _id: String,
        expedition_id: String,
        date: Date, // Date expedition
        drop: String, // egg / item / wild-pokemon / trainer
        pokemons: [{ // Pokemon yang dibawa player
            _id: String, // ID Dari pokemon Player
            entry_no: Number,
            name: String,
            level: Number,
            atk: Number,
            def: Number,
            sp_atk: Number,
            sp_def: Number,
            spd: Number,
            hp: Number,
            max_hp: Number,
            sp: Number,
            max_sp: Number,
            types: [String],
            gender: String,
            pokeball: String, // Item ID dari Pokeball
        }],
        // Jika egg
        egg: {
            egg_id: String,
            image_idx: String
        },
        // Jika Item
        items: [{
            item_id: String, // ID Item
            amount: Number
        }], 
        // Jika Wild Pokemon
        wild_pokemon: { // Detail pokemon yang ditemukan
            entry_no: Number,
            is_shiny: Boolean, // apakah shiny atau tidak
            level: Number,
            types: [String],
            gender: String,
            nature: String,
            atk: Number,
            def: Number,
            sp_atk: Number,
            sp_def: Number,
            spd: Number,
            hp: Number,
            max_hp: Number,
            sp: Number,
            max_sp: Number,
        },
        reward: { // JIka won-battle / caught
            exp: Number
        },
        status: String, // won-battle, lost-battle, caught, run
        // ]ika Trainer
        trainer:{
            name: String, // Nama Trainer 
            description: String, // Deskripsi Trainer
            image: String, // Foto Dari Trainer itu
            pokemons: {
                entry_no: Number,
                is_shiny: Boolean, // apakah shiny atau tidak
                level: Number,
                types: [String],
                gender: String,
                nature: String,
                atk: Number,
                def: Number,
                sp_atk: Number,
                sp_def: Number,
                spd: Number,
                hp: Number,
                max_hp: Number,
                sp: Number,
                max_sp: Number,
                reward: { // JIka won-battle / caught
                    gold: Number,
                    exp: Number
                },
            }
        },
        reward: { // JIka won-battle
            gold: Number,
            exp: Number
        },
        status: String, // won-battle, lost-battle, run
    }],
    multiplayers: [String], // ID Multiplayer semua multiplayer yang pernah dilakukan
    last_login: Date, // Login Terakhir
    status: String, // idle, expedition, room, matching, multiplayer
    // Jika expedition battle
    expedition_battle: {
        type: String, // trainer / wild-pokemon
        player: {
            active_pokemon: {  // Object Carried Pokemon
                _id: String, // ID Dari pokemon Player
                entry_no: Number,
                name: String,
                level: Number,
                atk: Number,
                def: Number,
                sp_atk: Number,
                sp_def: Number,
                spd: Number,
                hp: Number,
                max_hp: Number,
                sp: Number,
                max_sp: Number,
                types: [String],
                gender: String,
                pokeball: String, // Item ID dari Pokeball
            },
            inactive_pokemons: [{  // Object Carried Pokemon
                _id: String, // ID Dari pokemon Player
                entry_no: Number,
                name: String,
                level: Number,
                atk: Number,
                def: Number,
                sp_atk: Number,
                sp_def: Number,
                spd: Number,
                hp: Number,
                max_hp: Number,
                sp: Number,
                max_sp: Number,
                types: [String],
                gender: String,
                pokeball: String, // Item ID dari Pokeball
            }]
        },
        // Jika trainer
        trainer: {
            name: String, // Nama Trainer 
            description: String, // Deskripsi Trainer
            image: String, // Foto Dari Trainer itu
            active_pokemon: {
                entry_no: Number,
                is_shiny: Boolean, // apakah shiny atau tidak
                level: Number,
                types: [String],
                gender: String,
                nature: String,
                atk: Number,
                def: Number,
                sp_atk: Number,
                sp_def: Number,
                spd: Number,
                hp: Number,
                max_hp: Number,
                sp: Number,
                max_sp: Number,
                reward: { // JIka won-battle / caught
                    gold: Number,
                    exp: Number
                },
            },
            inactive_pokemons: [{
                entry_no: Number,
                is_shiny: Boolean, // apakah shiny atau tidak
                level: Number,
                types: [String],
                gender: String,
                nature: String,
                atk: Number,
                def: Number,
                sp_atk: Number,
                sp_def: Number,
                spd: Number,
                hp: Number,
                max_hp: Number,
                sp: Number,
                max_sp: Number,
                reward: { // JIka won-battle / caught
                    gold: Number,
                    exp: Number
                },
            }]
        },
        // Jika wild-pokemon
        wild_pokemon: { // Detail pokemon yang ditemukan
            entry_no: Number,
            is_shiny: Boolean, // apakah shiny atau tidak
            level: Number,
            types: [String],
            gender: String,
            nature: String,
            atk: Number,
            def: Number,
            sp_atk: Number,
            sp_def: Number,
            spd: Number,
            hp: Number,
            max_hp: Number,
            sp: Number,
            max_sp: Number,
            reward: { // JIka won-battle / caught
                exp: Number
            },
        },
    },
    // Jika sedang pada room
    room_id: String,
    // Jika sedang matching / multiplayer
    multiplayer_id: String
}

// Item apa saja yang ada pada Game
// Reference: Shop, Expedition, User
const Item = { 
    name: String,
    description: String,
    type: String, // potion, revive, pokeball, nutrient
    image: String,
    // Type 0 - Potion
    is_percentage: Boolean,
    amount_hp: Number, // Jika tdk full
    amount_sp: Number, // Jika tdk full
    // Type 1 - Revive
    is_percentage: Boolean,
    amount_hp: Number,
    // Type 2 - Pokeball
    catch_rate: Number,
    element: { // Opsional
        types: [String],
        catch_rate: Number
    },
    // Type 3 - Nutrient
    attribute: String, // hp, atk, def, sp-atk, sp-def
    amount: Number, // angka kecil
    // Type 4 - Stone
    element_type: String
}

// Egg, untuk menyimpan semua egg yang mungkin ditemukan
const Egg = {
    _id: String,
    name: String,
    description: String,
    price: Number,
    images: [String],
    explore_requirement: {
        min: Number,
        max: Number
    },
    shiny_rate: Number,
}

// Item yang dijual di Shop
const ShopItem = {
    _id: String,
    item_id: String, // Id dari Item yang dijual
    price: {
        buy: Number, // Harga jika dibeli
        sell: Number, // Harga jika dijual
    }
}

// Expedition
const Expedition = {
    _id: String,
    name: String,
    description: String,
    image: String,
    cost: Number, // Cost dalam explore
    drop: {
        rate: { // Dalam Ratio ex: 1:1:1:1
            egg: Number,
            item: Number,
            wild_pokemon: Number,
            trainer: Number
        },
        eggs: [{ // Bisa ketemu egg apa saja, egg_id
            egg_id: String, // ID Dari COllection Egg
            rate: Number, // dalam ratio
        }],
        items: [{// Bisa ketemu ITEM apa aja, item_id
            item_id: String,
            qty: {
                min: Number,
                max: Number
            }
        }], 
        wild_pokemons: [{ // Bisa ketemu POKEMON apa aja, 
            entry_no: String, // Pokedex Entry no
            level: {
                min: Number,
                max: Number,
            },
            reward: { // Reward jika pokemon dikalahkan / ditangkap
                exp: Number
            }
        }], 
        trainers: [{ // Bisa ketemu Trainer apa saja
            name: String, // Nama Trainer 
            description: String, // Deskripsi Trainer
            pokemon_brought: { // JUmlah Pokemon yang dibawa
                min: Number,
                max: Number
            },
            pokemons: [{ // Pokemon yang mungkin dibawa trainer
                entry_no: String, // PokeDex
                level: {
                    min: Number,
                    max: Number,
                },
                reward: {
                    gold: Number,
                    exp: Number
                }
            }],
            image: String, // Foto Dari Trainer itu
        }], 
    }
}

// Room
const Room = {
    _id: String, // 
    code: String, // Kode Ruangan (di generate program stringpanjang 5)
    password: String, // Password dari room
    is_public: Boolean, // Public tidaknya room
    host_id: String, // Host dari room ini
    // Jika ada Guest pada room ini
    guest_id: String, // ID Guest
}

// Bet
const Lobby = {
    _id: Number,
    name: String,
    description: String,
    image: String,
    bet: Number,
}

// Multiplayer
const Multiplayer = {
    _id: String,
    date: Date, // Kapan mulitplayer dimulai
    lobby_id: String, // opsional jika ranked
    player1: {
        user_id: String, // User ID
        pokemons: [{ // Object Carried Pokemon
            entry_no: Number,
            name: String,
            level: Number,
            atk: Number,
            def: Number,
            sp_atk: Number,
            sp_def: Number,
            spd: Number,
            hp: Number,
            max_hp: Number,
            sp: Number,
            max_sp: Number,
            types: [String],
            gender: String,
            pokeball: String, // Item ID dari Pokeball
        }],
        // Hanya ada ketika battle
        active_pokemon: {  // Object Carried Pokemon
            entry_no: Number,
            name: String,
            level: Number,
            atk: Number,
            def: Number,
            sp_atk: Number,
            sp_def: Number,
            spd: Number,
            hp: Number,
            max_hp: Number,
            sp: Number,
            max_sp: Number,
            types: [String],
            gender: String,
            pokeball: String, // Item ID dari Pokeball
        },
        inactive_pokemons: [{  // Object Carried Pokemon
            entry_no: Number,
            name: String,
            level: Number,
            atk: Number,
            def: Number,
            sp_atk: Number,
            sp_def: Number,
            spd: Number,
            hp: Number,
            max_hp: Number,
            sp: Number,
            max_sp: Number,
            types: [String],
            gender: String,
            pokeball: String, // Item ID dari Pokeball
        }]
    },
    player2: {
        user_id: String, // User ID
        pokemons: [{ // Object Carried Pokemon
            entry_no: Number,
            name: String,
            level: Number,
            atk: Number,
            def: Number,
            sp_atk: Number,
            sp_def: Number,
            spd: Number,
            hp: Number,
            max_hp: Number,
            sp: Number,
            max_sp: Number,
            types: [String],
            gender: String,
            pokeball: String, // Item ID dari Pokeball
        }],
        // Hanya ada ketika battle
        active_pokemon: {  // Object Carried Pokemon
            entry_no: Number,
            name: String,
            level: Number,
            atk: Number,
            def: Number,
            sp_atk: Number,
            sp_def: Number,
            spd: Number,
            hp: Number,
            max_hp: Number,
            sp: Number,
            max_sp: Number,
            types: [String],
            gender: String,
            pokeball: String, // Item ID dari Pokeball
        },
        inactive_pokemons: [{  // Object Carried Pokemon
            entry_no: Number,
            name: String,
            level: Number,
            atk: Number,
            def: Number,
            sp_atk: Number,
            sp_def: Number,
            spd: Number,
            hp: Number,
            max_hp: Number,
            sp: Number,
            max_sp: Number,
            types: [String],
            gender: String,
            pokeball: String, // Item ID dari Pokeball
        }]
    },
    is_ranked: Boolean,
    turn: Number, // Sekarang giliran ke brp. Ganjil player 1, Genap player 2
    status: String, // Status multiplayer. active, inactive
    // Jika ada pemenang
    winner: String, // User_id pemenang
    win_condition: String // Kondisi menang. opponent-lose, opponent-surender
}
