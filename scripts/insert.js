// Import
const mongoose = require('mongoose');
const { User, Expedition, Item, ShopItem,Egg } = require('../models');
const { ItemType, UserStatus, ElementType} = require('../variables');
require('dotenv').config()
// Helper Function
function insertLog(text){
    console.log(`Inserting ${text}...`)
}

// Main Program
async function main(){
    await mongoose.connect(process.env.MONGODB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });

    await mongoose.connection.dropDatabase()

    // Expedition
    // await Expedition.insertMany([
    //     {
    //         name: '',
    //         description: '',
    //         image: '',
    //         cost: 1, // Cost dalam explore
    //         drop: {
    //             rate: {
    //                 egg: 1,
    //                 item: 1,
    //                 wild_pokemon: 1,
    //                 trainer: 1
    //             },
    //             eggs: [
    //                 { egg_id: '', rate: 1}
    //             ],
    //             items: [
    //                 { item_id: '', qty: { min: 0, max: 5 } }
    //             ],
    //             wild_pokemons: [
    //                 {
    //                     entry_no: 1, 
    //                     level: { min:0, max:5 },
    //                     reward: { exp:100 }
    //                 }
    //             ],
    //             trainers: [
    //                 {
    //                     name: 'Trainer X',
    //                     description: 'This is trainer X',
    //                     pokemon_brought: { min:2, max:5 },
    //                     pokemons: [
    //                         { entry_no:1, level:{min:10,max:20}, reward:{gold:1000,exp:100} }
    //                     ],
    //                     image: ''
    //                 }
    //             ]
    //         },
    //     }
    // ])


    //function Potion || Ether || Revive
    function Healing(name,description,type,image,isPercentage,amountHp) {
        this.name = name;
        this.description = description;
        this.image = image;
        this.type = type;
        this.isPercentage = isPercentage;
        this.amount = amountHp;
    };

     //Function Pokeball
    function Pokeball(name,description,image,catchRate) {
        this.name = name;
        this.description = description;
        this.image = image;
        this.type = ItemType.POKEBALL;
        this.catchRate = catchRate;
    };

    function ElementalPokeball(pokeball,types,catchRate) {
        for (const key in pokeball) {
            this[key] = pokeball[key]
        }
        this.element = {types,catchRate};
    };

    // Function Nutrient
    function Nutrient(name,description,image,attribute,amount) {
        this.name = name;
        this.description = description;
        this.image = image;
        this.type = ItemType.NUTRIENT;
        this.attribute = attribute;
        this.amount = amount;
    };   
    
      // Function Stone
    function Stone(name,description,image,elementType) {
        this.name = name;
        this.description = description;
        this.image = image;
        this.type = ItemType.STONE;
        this.elementType = elementType;
    };   
    
    
    // Insert Item
    insertLog('Item')
    const arr = [];
    // Insert Potion
    arr.push(new Healing("potion","Restores 20 HP.",ItemType.POTION,"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/potion.png",false,20));
    arr.push(new Healing("max-potion","Restores HP to full",ItemType.POTION,"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/max-potion.png",true,100));
    //Insert Ether
    arr.push(new Healing("ether","Restores 10 SP",ItemType.ETHER,"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/ether.png",false,10));
    arr.push(new Healing("max-ether","Restores SP to full",ItemType.ETHER,"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/max-ether.png",true,100));
    //Insert Revive
    arr.push(new Healing("revive","Revives with half HP.",ItemType.REVIVE,"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/revive.png",true,50))
    arr.push(new Healing("max-revive", "Revives with full HP.",ItemType.REVIVE,"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/max-revive.png",true,100))
    //Insert Pokeball
    arr.push(new Pokeball("poke-ball","Tries to catch a wild Pokémon.","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/poke-ball.png",25));
    //Insert ElementalPokeball
    arr.push(new ElementalPokeball(new Pokeball("poke-ball","Tries to catch a wild Pokémon.", "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/poke-ball.png",25),[ElementType.FIRE,ElementType.FLYING],55));
    //Insert Nutrient
    arr.push(new Nutrient("hp-up","Increases HP effort by 10","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/hp-up.png","hp",10));
    //Insert Stone 
    arr.push(new Stone("fire-stone","Evolve fire pokemon","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/fire-stone.png",ElementType.FIRE));        
    // console.log(potion)
    const modelArr = []
    for(let i=0;i<arr.length;i++){
        const newItem = new Item(arr[i])
        modelArr.push(newItem)
        await newItem.save()
    }

    // Insert ShopItem
    insertLog('Shop Item')
    await ShopItem.insertMany([
        //potion
        { itemId: modelArr[0]._id, price: { buy: 200, sell: 100 } },
        { itemId: modelArr[1]._id, price: { buy: 2500, sell: 1250 } },
        //sp
        { itemId: modelArr[2]._id, price: { buy: 300, sell: 600 } },
        { itemId: modelArr[3]._id, price: { buy: 3000, sell: 1150 } },
        //revive
        { itemId: modelArr[4]._id, price: { buy: 2000, sell: 1000 } },
        { itemId: modelArr[5]._id, price: { buy: 4000, sell: 2000 } },
        //pokeball
        { itemId: modelArr[6]._id, price: { buy: 200, sell: 100 } },
        { itemId: modelArr[7]._id, price: { buy: 1000, sell: 500 } },
        //nutrient
        { itemId: modelArr[8]._id, price: { buy: 10000, sell: 5000 } },
        //Stone
        { itemId: modelArr[9]._id, price: { buy: 5000, sell: 0 } },
    ])

    // Insert User
    insertLog('User')
    function UserObj(username, password){
        this.username = username
        this.password = password
        this.image = 'http://'
        this.money = 1000
        this.isPremium = false
        this.exploreCount = 0
        this.remainingExplore = 0
        this.multiplayerCount = 0
        this.winrate = 0
        this.startingDate = new Date()
        this.pokemons = []
        this.items = []
        this.eggs = []
        this.pokedex = {}
        this.expeditions = []
        this.multiplayers = []
        this.lastLogin = new Date()
        this.status = UserStatus.IDLE
    }
    const users = [
        new UserObj('davin0','davin0'),     
        new UserObj('davin1','davin1'),     
        new UserObj('davin2','davin2'),
    ]
    for(let i=0;i<users.length;i++){
        users[i] = new User(users[i])
        await users[i].save()
    }

    insertLog('Egg')
    //insert egg
    function FunEgg(name,description,price,images,min,max,shinyRate) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.images = images;
        this.exploreRequirement = {min,max};
        this.shinyRate = shinyRate;
    };

    await Egg.insertMany([
        new FunEgg("Normal-Egg","This is a normal egg",1000,["/assets/images/egg/egg1","/assets/images/egg/egg2","/assets/images/egg/egg3","/assets/images/egg/egg4"],3,10,20),
        new FunEgg("Rare-Egg","This is a rare egg",2500,["/assets/images/egg/egg5","/assets/images/egg/egg6","/assets/images/egg/egg7","/assets/images/egg/egg8"],10,20,30),
        new FunEgg("Super-rare-Egg","This is a super rare egg",3800,["/assets/images/egg/egg9","/assets/images/egg/egg10","/assets/images/egg/egg11","/assets/images/egg/egg12"],25,50,50),
        new FunEgg("Mythic-Egg","This is a mythic egg",4800,["/assets/images/egg/egg13","/assets/images/egg/egg14","/assets/images/egg/egg15","/assets/images/egg/egg16"],55,70,60),
        new FunEgg("Legendary-Egg","This is a legendary egg",6800,["/assets/images/egg/egg17","/assets/images/egg/egg18","/assets/images/egg/egg19","/assets/images/egg/egg20"],70,90,80),
    ])

    
    // Exit
    console.log('Insert Sukses!')
    process.exit()
}

main()
