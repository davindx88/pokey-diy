// Import
const mongoose = require('mongoose');
const { User, Expedition, Item, ShopItem,Egg } = require('../models');
const { ItemType, UserStatus, ElementType} = require('../variables');
require('dotenv').config()

// Helper Function
function insertLog(text){
    console.log(`Inserting ${text}...`)
}

// Main Program
async function main(){
    await mongoose.connect(process.env.MONGODB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
    
    // Insert Expedition
    insertLog('Expedition')
    await Expedition.deleteMany()
    await Expedition.insertMany([
        {
            name: 'Beginner Expedition',
            description: 'Expedition for beginner trainer',
            image: 'https://c4.wallpaperflare.com/wallpaper/927/247/737/pokemon-nature-pikachu-charmander-wallpaper-preview.jpg',
            cost: 1, // Cost dalam explore
            drop: {
                rate: { egg: 1, item: 1, wildPokemon: 1, trainer: 1},
                eggs: [
                    { eggId: '60ba0c8829b6091ddc452443', rate: 3},
                    { eggId: '60ba0c8829b6091ddc452444', rate: 2},
                    { eggId: '60ba0c8829b6091ddc452445', rate: 1},
                ],
                items: [
                    { itemId: '60ba0c8729b6091ddc45242c', qty: { min: 0, max: 5 } },
                    { itemId: '60ba0c8729b6091ddc45242e', qty: { min: 0, max: 5 } },
                    { itemId: '60ba0c8729b6091ddc452435', qty: { min: 0, max: 2 } },
                ],
                wildPokemons: [
                    { entryNo: 1, level: { min:1, max:5 }, exp: 100 },
                    { entryNo: 25, level: { min:1, max:5 }, exp: 100 },
                    { entryNo: 166, level: { min:1, max:5 }, exp: 100 },
                    { entryNo: 240, level: { min:1, max:5 }, exp: 100 },
                    { entryNo: 366, level: { min:1, max:5 }, exp: 100 },
                    { entryNo: 368, level: { min:1, max:5 }, exp: 100 },
                ],
                trainers: [
                    '60ccbd0de1332336900caa40'
                ]
            },
        }
    ])

    // Exit
    console.log('Insert Sukses!')
    process.exit()
}

main()
