// Import
const { default: axios } = require('axios');
const mongoose = require('mongoose');
const { PokedexPokemon, PokeEvolve } = require('../models');
require('dotenv').config()
// Helper Function
function insertLog(text){
    console.log(`Inserting ${text}...`)
}

// Main Program
async function insertPokedexPokemon(){
    await PokedexPokemon.deleteMany()
    // Insert PokedexPokemon
    insertLog('PokedexPokemon')
    const nPokemon = 898
    for(let i=1;i<=nPokemon;i++){
        const entryNo = i
        const url = `https://pokeapi.co/api/v2/pokemon/${entryNo}`
        const {data} = await axios.get(url)
        
        let { name, stats, types, sprites, height, weight } = data
        // Status Pokemon
        const atk = [...stats].find(({stat}) => stat.name === 'attack').base_stat
        const def = [...stats].find(({stat}) => stat.name === 'defense').base_stat
        const spAtk = [...stats].find(({stat}) => stat.name === 'special-attack').base_stat
        const spDef = [...stats].find(({stat}) => stat.name === 'special-defense').base_stat
        const spd = [...stats].find(({stat}) => stat.name === 'speed').base_stat
        const maxHp = [...stats].find(({stat}) => stat.name === 'hp').base_stat
        const maxSp = Math.max(Math.floor(maxHp/5), 10)
        // Type
        types = types.map(({type}) => type.name)
        // Create Instance
        const pokedexPokemon = new PokedexPokemon({
            name, 
            entryNo, 
            types, 
            atk, 
            def, 
            spAtk, 
            spDef, 
            spd, 
            maxHp, 
            maxSp,
            image: {
                male: sprites.front_default,
                female: sprites.front_female,
                shinyMale: sprites.front_shiny,
                shinyFemale: sprites.front_shiny_female,
            },
            height,
            weight
        });
        await pokedexPokemon.save();
        console.log(`[PokedexPokemon] Insert ${entryNo}`)
    }
}

async function insertPokeEvolve(){
    await PokeEvolve.deleteMany()
    // Evolve Tree
    insertLog('Evolve')
    const nEvolve = 467
    let evolveIdx = 0;
    for(let i=1;i<=nEvolve;i++){
        evolveIdx++;
        const url = `https://pokeapi.co/api/v2/evolution-chain/${evolveIdx}`
        try{
            const { data:{chain} } = await axios.get(url)
            async function recur(chain, fromEvolveId, depth=0){
                const { evolves_to, species } = chain
                const evolveNode = new PokeEvolve()
                // Attribute
                const [entryNo] = species.url
                    .replace('https://pokeapi.co/api/v2/pokemon-species/', '')
                    .split('/');
                console.log(`[Evolve] ${entryNo} ${species.name}!`)
                const pokemon = await PokedexPokemon.findOne({entryNo})
                evolveNode.pokemon = pokemon._id;
                if(fromEvolveId != null){
                    evolveNode.evolveFrom = fromEvolveId;
                    evolveNode.requirements = [...pokemon.types].map(element => ({element, amount: depth*3}))
                }
                // Save for Reference
                await evolveNode.save()
                // Get Child
                evolveNode.evolveTo = []
                for(let i=0;i<evolves_to.length;i++){
                    const childNode = await recur(evolves_to[i], evolveNode._id, depth+1);
                    evolveNode.evolveTo.push(childNode._id)                
                }
                await evolveNode.save()
                
                return evolveNode
            }
            await recur(chain, null)
        }catch{
            i--;
        }
    }
}

async function main(){
    await mongoose.connect(process.env.MONGODB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });

    // await PokedexPokemon.deleteMany()
    
    // await insertPokedexPokemon()
    await insertPokeEvolve()
    
    // Exit
    console.log('Insert Sukses!')
    process.exit()
}

main()
