// Import
const mongoose = require('mongoose');
const { ExpeditionTrainer } = require('../models');
require('dotenv').config()

// Helper Function
function insertLog(text){
    console.log(`Inserting ${text}...`)
}

// Main Program
async function main(){
    await mongoose.connect(process.env.MONGODB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
    
    // Insert Expedition
    insertLog('Expedition Trainer')
    await ExpeditionTrainer.deleteMany()
    await ExpeditionTrainer.insertMany([
        {
            name: 'Easy Trainer',
            description: 'This is trainer Easy',
            pokemonBrought: { min:2, max:3 },
            pokemons: [1,4,7,10,11],
            levelRange: { min: 1, max: 10 },
            image: 'https://p.kindpng.com/picc/s/163-1638120_pokemon-trainer-sprite-pixel-sprites-trainers-pokemon-hd.png',
            reward: { gold: 100, exp: 100 }
        },
        {
            name: 'Medium Trainer',
            description: 'This is trainer Medium',
            pokemonBrought: { min:3, max:5 },
            pokemons: [2,5,8,10,11],
            levelRange: { min: 20, max: 30 },
            image: 'https://pokecharms.com/data/attachment-files/2018/08/656072_Trainer_pic2_2_00000.png',
            reward: { gold: 1000, exp: 1000 }
        },
        {
            name: 'Expert Trainer',
            description: 'Trainer that expert at pokemon battles',
            pokemonBrought: { min:5, max:6 },
            pokemons: [3,6,9,203,123],
            levelRange: { min: 50, max: 60 },
            image: 'https://pokecharms.com/data/attachment-files/2017/11/573332_200TrainerN.png',
            reward: { gold: 10000, exp: 10000 }
        },
    ])

    // Exit
    console.log('Insert Sukses!')
    process.exit()
}

main()
