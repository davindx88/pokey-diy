const mongoose = require("mongoose");
const { PlayerPokemon, WildPokemon, WildTrainer } = require("./types");
const { Schema } = mongoose;

// Schema
const userSchema = new Schema({
	username: { type: String, required: true },
	password: { type: String, required: true },
	image: { type: String, required: true },
	money: { type: Number, required: true },
	isPremium: { type: Boolean, required: true },
	exploreCount: { type: Number, required: true }, // Total Explore selamat ini
	remainingExplore: { type: Number, required: true }, // Explore hari ini
	multiplayerCount: { type: Number, required: true }, // Total Multiplayer selama ini
	winrate: { type: Number, required: true }, // Winrate dari Multiplayer
	startingDate: { type: Date, required: true }, // User buat account kapan
	pokemons: { type: [PlayerPokemon], required: true },
	items: {
		type: [
			{
				itemId: {
					type: Schema.Types.ObjectId,
					ref: "Item",
					required: true,
				}, // Id Itemnya apa
				qty: { type: Number, required: true }, // Punya sebanyak berapa
			},
		],
		required: true,
	},
	eggs: {
		type: [
			{
				eggId: {
					type: Schema.Types.ObjectId,
					ref: "Egg",
					required: true,
				}, // ID Dari Egg yang ditemukan
				image: { type: String, required: true },
				exploreRemaining: { type: Number, required: true }, // Kurang explore berapa kali
			},
		],
		required: true,
	},
	pokedex: { type: Map, of: String, required: true }, // Key: entry no, Value({ type: String, required: true}): seen / caught
	expeditions: {
		// Semua expedition yang pernah dilakukan
		type: [
			{
				expeditionId: {
					type: Schema.Types.ObjectId,
					ref: "Expedition",
					required: true,
				},
				date: { type: Date, required: true }, // Date expedition
				drop: { type: String, required: true }, // egg / item / wild-pokemon / trainer
				pokemons: { type: [PlayerPokemon], required: true },
				// Jika egg
				egg: {
					type: {
						eggId: {
							type: Schema.Types.ObjectId,
							ref: "Egg",
							required: true,
						},
						image: { type: String, required: true },
					},
					required: false,
				},
				// Jika Item
				items: {
					type: [
						{
							itemId: {
								type: Schema.Types.ObjectId,
								ref: "Item",
								required: true,
							}, // ID Item
							amount: { type: Number, required: true },
						},
					],
					required: false,
					default: undefined
				},
				// Jika Wild Pokemon
				wildPokemon: { type: WildPokemon, required: false },
				// ]ika Trainer
				trainer: { type: WildTrainer, required: false },
				// Jika wildpokemon / trainer
				reward: {
					// JIka won-battle
					type: {
						gold: { type: Number, required: true },
						exp: { type: Number, required: true },
					},
					required: false,
				},
				moves: { // Untuk mencatat history move
					type: [{ 
						index: { type: Number, required:true },
						log: { type: String, required:true }, 
					}],
					required: true,
				},
				status: { type: String, required: false }, // won-battle, lost-battle, run, caught
			},
		],
		required: true,
	},
	multiplayers: {
		// ID Multiplayer semua multiplayer yang pernah dilakukan
		type: [{ type: Schema.Types.ObjectId, ref: "Multiplayer" }],
		required: true,
	},
	lastLogin: { type: Date, required: true }, // Login Terakhir
	status: { type: String, required: true }, // idle, expedition, room, matching, multiplayer
	// Jika expedition battle
	expeditionBattle: {
		type:{
			historyId: { type: Schema.Types.ObjectId, required: true },
			expeditionType: { type: String, required: true }, // trainer / wild-pokemon
			player: {
				type: {
					activePokemon: { type: PlayerPokemon, required: true },
					inactivePokemons: {
						type: [PlayerPokemon],
						required: true,
					},
				},
				required: true,
			},
			moves: { // Untuk mencatat history move
				type: [{ 
					index: { type: Number, required:true },
					log: { type: String, required:true }, 
				}],
				required: true,
			},
			// Jika trainer
			trainer: { type: WildTrainer, required: false },
			// Jika wild-pokemon
			wildPokemon: { type: WildPokemon, required: false },
			// Status
			status: { type: String, required: true }, // won-battle, lost-battle, run, caught
		},
		required: false,
	},
	// Jika sedang pada room
	roomId: { type: Schema.Types.ObjectId, ref: "Room", required: false },
	// Jika sedang matching / multiplayer
	multiplayer: {
		type: Schema.Types.ObjectId,
		ref: "Multiplayer",
		required: false,
	},
});

// Model
const User = mongoose.model("User", userSchema);

// Export
module.exports = User;
