const BattlePokemon = require("./battlePokemon");

const TrainerPokemon = {
	...BattlePokemon,
	reward: { // JIka won-battle / caught
		type:{ 
			gold: { type: Number, required: true },
			exp: { type: Number, required: true },
		},
		required: true,
	},
};

module.exports = TrainerPokemon;
