const Pokemon = {
	name: { type: String, required: true },
	entryNo: { type: Number, required: true },
	types: { type: [String], required: true },
	atk: { type: Number, required: true },
	def: { type: Number, required: true },
	spAtk: { type: Number, required: true },
	spDef: { type: Number, required: true },
	spd: { type: Number, required: true },
	maxHp: { type: Number, required: true },
	maxSp: { type: Number, required: true },
};

module.exports = Pokemon;
