const Pokemon = require("./pokemon");
const BattlePokemon = require("./battlePokemon");
const CarriedPokemon = require("./carriedPokemon");
const PlayerPokemon = require("./playerPokemon");
const TrainerPokemon = require("./trainerPokemon");
const WildPokemon = require("./wildPokemon");
const WildTrainer = require("./wildTrainer");
const Trainer = require("./trainer");
const RangeMinMax = require("./rangeMinMax")

module.exports = {
	Pokemon,
	BattlePokemon,
	CarriedPokemon,
	PlayerPokemon,
	TrainerPokemon,
	WildPokemon,
	WildTrainer,
	Trainer,
	RangeMinMax
};
