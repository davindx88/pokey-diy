const RangeMinMax = {
	min: { type: Number, required: true },
	max: { type: Number, required: true },
};

module.exports = RangeMinMax;
