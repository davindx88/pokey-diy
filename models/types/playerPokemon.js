const CarriedPokemon = require("./carriedPokemon");

const PlayerPokemon = {
	...CarriedPokemon,
	exp: { type: Number, required: true },
	maxExp: { type: Number, required: true },
	isActive: { type: Boolean, required: true }, // Jika tidak active maka pokemon sudah dijual
};

module.exports = PlayerPokemon;
