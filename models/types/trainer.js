const mongoose = require("mongoose");
const { Schema } = mongoose;
const CarriedPokemon = require("./carriedPokemon");

const Trainer = {
	userId: { type: Schema.Types.ObjectId, ref: "User", required: true }, // User ID
	pokemons: { type: [CarriedPokemon], required: true },
	// Hanya ada ketika battle
	activePokemon: { type: CarriedPokemon, required: false },
	inactivePokemons: { type: [CarriedPokemon], required: false },
	action: { type: String, required: false }
};

module.exports = Trainer;
