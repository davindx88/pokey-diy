const TrainerPokemon = require("./trainerPokemon");

const WildTrainer = {
	name: { type: String, required: true }, // Nama Trainer
	description: { type: String, required: true }, // Deskripsi Trainer
	image: { type: String, required: true }, // Foto Dari Trainer itu
	pokemons: { type: [TrainerPokemon], required: true },
};

module.exports = WildTrainer;
