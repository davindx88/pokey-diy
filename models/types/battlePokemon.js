const Pokemon = require("./pokemon");

const BattlePokemon = {
    ...Pokemon,
	level: { type: Number, required: true },
	isShiny: { type: Boolean, required: true }, // apakah shiny atau tidak
	gender: { type: String, required: true },
	nature: { type: String, required: true },
	hp: { type: Number, required: true },
	sp: { type: Number, required: true },
};

module.exports = BattlePokemon;
