const mongoose = require("mongoose");
const { Schema } = mongoose;
const BattlePokemon = require("./battlePokemon");

const CarriedPokemon = {
	...BattlePokemon,
	_id: { type: Schema.Types.ObjectId, required: true }, // Object ID
	pokeball: { type: Schema.Types.ObjectId, ref: "Item", required: true }, // Item ID dari Pokeball
};

module.exports = CarriedPokemon;
