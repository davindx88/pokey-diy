const BattlePokemon = require("./battlePokemon");

const WildPokemon = {
	...BattlePokemon,
	reward: { // Reward yang bisa didapat
		type: { 
			exp: { type: Number, required: true },
		},
		required: true,
	},
};

module.exports = WildPokemon;
