const mongoose = require("mongoose");
const { RangeMinMax } = require("./types");
const { Schema } = mongoose;

// Schema
const eggSchema = new Schema({
	name: { type: String, required: true },
	description: { type: String, required: true },
	price: { type: Number, required: true },
	images: { type: [String], required: true },
	exploreRequirement: { type: RangeMinMax, required: true },
	shinyRate: { type: Number, required: true },
});

// Model
const Egg = mongoose.model("Egg", eggSchema);

// Export
module.exports = Egg;
