const mongoose = require('mongoose');
const { Trainer } = require('./types')
const { Schema } = mongoose;

// Schema
const MultiplayerSchema = new Schema({
	date: { type: Date, required: true }, // Kapan mulitplayer dimulai
	player1: { type: Trainer, required: true },
	player2: { type: Trainer, required: true },
	isRanked: { type: Boolean, required: true },
	status: { type: String, required: true }, // Status multiplayer. active, inactive
	winner: { type: String, required: false }, // User_id pemenang
	moves: { // Untuk mencatat history move
		type: [{ 
			index: { type: Number, required:true },
			log: { type: String, required:true }, 
		}],
		required: true,
	},
});

// Model
const Multiplayer = mongoose.model("Multiplayer", MultiplayerSchema);

// Export
module.exports = Multiplayer;
