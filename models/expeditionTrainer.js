const mongoose = require("mongoose");
const { RangeMinMax } = require("./types");
const { Schema } = mongoose;

// Schema
const expeditionTrainerSchema = new Schema({
    name: { type: String, required: true }, // Nama Trainer
	description: { type: String, required: true }, // Deskripsi Trainer
	image: { type: String, required: true }, // Foto Dari Trainer itu
    pokemonBrought: { type: RangeMinMax, required: true }, // Jumlah Pokemon yang dibawa
    pokemons: { type: [Number], required: true }, // No Entry dari pokemon yang bisa dibawa
    levelRange: { type: RangeMinMax, required: true }, // Range level pokemon
    reward: { // Reward per pokemon
        type: {
            gold: { type: Number, required: true},
            exp: { type: Number, required: true},
        }
    }
});

// Model
const ExpeditionTrainer = mongoose.model("ExpeditionTrainer", expeditionTrainerSchema);

// Export
module.exports = ExpeditionTrainer;
