const mongoose = require('mongoose');
const { Schema } = mongoose;

// Schema
const shopItemSchema = new Schema({
	itemId: { type: Schema.Types.ObjectId, ref: "Item", required: true }, // Id dari Item yang dijual
	price: {
		type: {
			buy: { type: Number, required: true }, // Harga jika dibeli
			sell: { type: Number, required: true }, // Harga jika dijual
		},
		required: true,
	},
});

// Model
const ShopItem = mongoose.model("ShopItem", shopItemSchema);

// Export
module.exports = ShopItem;
