const mongoose = require('mongoose');
const { Pokemon } = require('./types');
const { Schema } = mongoose;

// Schema
const pokedexPokemonSchema = new Schema({
    ...Pokemon,
    image: { 
        type: { // Jika genderless maka pakai yg male
            male: { type: String, required: true }, 
            female: { type: String, required: true },
            shinyMale: { type: String, required: true },
            shinyFemale: { type: String, required: true },
        },
        required: true
    },
    height: { type: Number, required: true },
    weight: { type: Number, required: true },
});

// Model
const PokedexPokemon = mongoose.model("PokedexPokemon", pokedexPokemonSchema);

// Export
module.exports = PokedexPokemon;
