const mongoose = require("mongoose");
const { RangeMinMax, WildPokemon, WildTrainer } = require("./types");
const { Schema } = mongoose;

// Schema
const expeditionSchema = new Schema({
	name: { type: String, required: true },
	description: { type: String, required: true },
	image: { type: String, required: true },
	cost: { type: Number, required: true }, // Cost dalam explore
	drop: {
		type: {
			rate: {
				// Dalam Ratio ex: 1:1:1:1
				type: {
					egg: { type: Number, required: true },
					item: { type: Number, required: true },
					wildPokemon: { type: Number, required: true },
					trainer: { type: Number, required: true },
				},
				required: true,
			},
			eggs: {
				type: [
					{
						// Bisa ketemu egg apa saja, egg_id
						eggId: {
							type: Schema.Types.ObjectId,
							ref: "Egg",
							required: true,
						}, // ID Dari COllection Egg
						rate: { type: Number, required: true }, // dalam ratio
					},
				],
				required: true,
			},
			items: {
				type: [
					{
						// Bisa ketemu ITEM apa aja, item_id
						itemId: {
							type: Schema.Types.ObjectId,
							ref: "Item",
							required: true,
						},
						qty: { type: RangeMinMax, required: true },
					},
				],
				required: true,
			},
			wildPokemons: { 
				type: [{
					entryNo: { type: Number, required: true},
					level: { type: RangeMinMax, required: true},
					exp: { type: Number, required: true }
				}], 
				required: true 
			},
			trainers: { 
				type: [Schema.Types.ObjectId],  // Reference Expedition Trainer
				required: true 
			},
		},
		required: true,
	},
});

// Model
const Expedition = mongoose.model("Expedition", expeditionSchema);

// Export
module.exports = Expedition;
