const User = require("./user");
const PokedexPokemon = require("./pokedexPokemon");
const PokeEvolve = require("./pokeEvolve");
const Item = require("./item");
const ShopItem = require("./shopitem");
const Egg = require("./egg");
const Expedition = require("./expedition");
const ExpeditionTrainer = require("./expeditionTrainer");
const Room = require("./room");
const Lobby = require("./lobby");
const Multiplayer = require("./multiplayer");

module.exports = {
	User,
	PokedexPokemon,
	PokeEvolve,
	Item,
	ShopItem,
	Egg,
	Expedition,
	ExpeditionTrainer,
	Room,
	Lobby,
	Multiplayer,
};
