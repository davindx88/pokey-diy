const mongoose = require('mongoose');
const { Schema } = mongoose;

// Schema
const LobbySchema = new Schema({
	name: { type: String, required: true },
	description: { type: String, required: true },
	image: { type: String, required: true },
	bet: { type: Number, required: true },
});

// Model
const Lobby = mongoose.model("Lobby", LobbySchema);

// Export
module.exports = Lobby;
