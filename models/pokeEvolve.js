const mongoose = require("mongoose");
const { Schema } = mongoose;

// Schema
const pokeEvolveSchema = new Schema({
	pokemon: { type: Schema.Types.ObjectId, ref: 'PokedexPokemon', required: true },
	evolveTo: { type: [{type: Schema.Types.ObjectId, ref: 'PokeEvolve'}], required: true },
	evolveFrom: { type: Schema.Types.ObjectId, ref: 'PokeEvolve', required: false },
	requirements: { 
		type: [{ 
			element: { type: String, required: true },
			amount: { type: Number, required: true },
		}], 
		required: false 
	}
});

// Model
const PokeEvolve = mongoose.model("PokeEvolve", pokeEvolveSchema);

// Export
module.exports = PokeEvolve;
