const mongoose = require('mongoose');
const { Schema } = mongoose;

// Schema
const itemSchema = new Schema({
	name: { type: String, required: true },
	description: { type: String, required: true },
	image: { type: String, required: true },
	type: { type: String, required: true }, // potion, revive, pokeball, nutrient
	// Type 0 - Potion || Type 1 - ETHER  || Type 2 - Revive
	isPercentage: { type: Boolean, required: false },
	amount: { type: Number, required: false }, // Jika tdk full dan untuk percentage juga (heal berapa persen hp)
	// Type 3 - Pokeball
	catchRate: { type: Number, required: false },
	element: { // Opsional
		type: {
			types: { type: [String], required: true },
			catchRate: { type: Number, required: true },
		},
		required: false,
	},
	// Type 4 - Nutrient
	attribute: { type: String, required: false }, // hp, atk, def, sp-atk, sp-def
	amount: { type: Number, required: false }, // angka kecil
	// Type 5 - Stone
	elementType: { type: String, required: false },
});

// Model
const Item = mongoose.model("Item", itemSchema);

// Export
module.exports = Item;
