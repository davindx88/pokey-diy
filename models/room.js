const mongoose = require('mongoose');
const { Schema } = mongoose;

// Schema
const RoomSchema = new Schema({
	name: { type: String, required: true },
	code: { type: String, required: true }, // Kode Ruangan (di generate program stringpanjang 5)
	password: { type: String, required: true }, // Password dari room
	isPublic: { type: Boolean, required: true }, // Public tidaknya room
	hostId: { type: Schema.Types.ObjectId, ref: "User", required: true }, // Host dari room ini
	hostPokemons: { type: [Schema.Types.ObjectId], required: false },
	// Jika ada Guest pada room ini
	guestId: { type: Schema.Types.ObjectId, ref: "User", required: false }, // ID Guest
	guestPokemons: { type: [Schema.Types.ObjectId], required: false }
});

// Model
const Room = mongoose.model("Room", RoomSchema);

// Export
module.exports = Room;
